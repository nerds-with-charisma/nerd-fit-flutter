import 'package:flutter/material.dart';
import 'package:nerd_fit/components/AddShoppingListItem.dart';
import 'package:nerd_fit/components/ShoppingItemListView.dart';
import 'package:nerd_fit/utils/statics.dart';

class ShoppingListScreen extends StatefulWidget {
  static const routeName = '/shopping';

  ShoppingListScreen({
    this.filter,
    this.sort,
    this.descending,
  });

  final bool filter;
  final String sort;
  final bool descending;

  @override
  State<ShoppingListScreen> createState() => _ShoppingListScreenState();
}

class _ShoppingListScreenState extends State<ShoppingListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(statics()['standardBackground']),
            fit: BoxFit.cover,
          ),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 8,
                  offset: const Offset(0, 0),
                ),
              ],
            ),
            child: Column(
              children: <Widget>[
                ShoppingItemListView(
                  filter: widget.filter,
                  sort: widget.sort,
                  descending: widget.descending,
                ),
                AddShoppingListItem(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
