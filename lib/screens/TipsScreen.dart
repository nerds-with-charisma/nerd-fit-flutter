import 'package:flutter/material.dart';
import 'package:nerd_fit/app/tips/TipsTabs.dart';

class TipsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Expanded(child: TipsTabs()),
        ],
      ),
    );
  }
}
