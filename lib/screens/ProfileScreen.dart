import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:nerd_fit/components/MarginBottom.dart';
import 'package:nerd_fit/components/PlatformExceptionAlert.dart';
import 'package:nerd_fit/components/SubmitButton.dart';
import 'package:nerd_fit/models/UserDataModel.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:nerd_fit/screens/PlanCard.dart';
import 'package:provider/provider.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ProfileScreen extends StatefulWidget {
  static const routeName = '/profile';

  const ProfileScreen({Key key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String genderDropdown = 'Male';
  double ageSlider = 21;
  double weightSlider = 175;
  //double workoutsPerWeek = 3;
  //double cardioPerWeek = 3;
  bool hasLoadedOnce = false;
  String goal = 'Hypertrophy';

  double squatORM = 45;
  double benchORM = 45;
  double deadliftORM = 45;
  double calfRaisesORM = 45;
  double rowORM = 45;
  double curlORM = 5;
  double uppercutORM = 5;
  double gluteBridgeORM = 45;
  double pressORM = 45;
  double closeORM = 45;
  double rdlORM = 45;
  double inclineORM = 45;
  double latORM = 5;
  double shrugORM = 45;

  @override
  Widget build(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);

    // not  using this  for now. Keep it tho
    // String _setGoal(goalTxt) {
    //   if (goalTxt == 'Hypertrophy') return 'Build lean muscle';
    //   if (goalTxt == 'Endurance') return 'Focus on form';
    //   if (goalTxt == 'Strength') return 'Get Buff!!!';

    //   return 'Muscle Growth';
    // }

    Future<dynamic> _readJson(goalTxt) async {
      // load json
      String jsonToLoad;

      if (goalTxt == 'Hypertrophy') {
        jsonToLoad =
            await rootBundle.loadString('assets/HypertrophyWorkoutModel.json');
      } else if (goalTxt == 'Pyramid') {
        jsonToLoad =
            await rootBundle.loadString('assets/PyramidWorkoutModel.json');
      } else if (goalTxt == 'Endurance') {
        jsonToLoad =
            await rootBundle.loadString('assets/EnduranceWorkoutModel.json');
      } else if (goalTxt == 'Push Pull Legs') {
        jsonToLoad =
            await rootBundle.loadString('assets/PushPullLegsWorkout.json');
      } else {
        jsonToLoad =
            await rootBundle.loadString('assets/StrengthWorkoutModel.json');
      }
      final data = await json.decode(jsonToLoad);
      return data;
    }

    void getUserData() async {
      final userData = await database.getUserDataStream().first;
      print(userData);

      if (userData != null) {
        if (userData[0].gender != null)
          setState(() => genderDropdown = userData[0].gender);
        if (userData[0].age != null)
          setState(() => ageSlider = userData[0].age);
        if (userData[0].weight != null)
          setState(() => weightSlider = userData[0].weight);
        if (userData[0].goal != null) setState(() => goal = userData[0].goal);

        if (userData[0].squatORM != null)
          setState(() => squatORM = userData[0].squatORM);
        if (userData[0].benchORM != null)
          setState(() => benchORM = userData[0].benchORM);
        if (userData[0].deadliftORM != null)
          setState(() => deadliftORM = userData[0].deadliftORM);
        if (userData[0].calfRaisesORM != null)
          setState(() => calfRaisesORM = userData[0].calfRaisesORM);
        if (userData[0].rowORM != null)
          setState(() => rowORM = userData[0].rowORM);
        if (userData[0].curlORM != null)
          setState(() => curlORM = userData[0].curlORM);
        if (userData[0].uppercutORM != null)
          setState(() => uppercutORM = userData[0].uppercutORM);
        if (userData[0].gluteBridgeORM != null)
          setState(() => gluteBridgeORM = userData[0].gluteBridgeORM);
        if (userData[0].pressORM != null)
          setState(() => pressORM = userData[0].pressORM);
        if (userData[0].closeORM != null)
          setState(() => closeORM = userData[0].closeORM);
        if (userData[0].rdlORM != null)
          setState(() => rdlORM = userData[0].rdlORM);
        if (userData[0].inclineORM != null)
          setState(() => inclineORM = userData[0].inclineORM);
        if (userData[0].latORM != null)
          setState(() => latORM = userData[0].latORM);
        if (userData[0].shrugORM != null)
          setState(() => shrugORM = userData[0].shrugORM);

        // if (userData[0].workoutsPerWeek != null)
        //   setState(() => workoutsPerWeek = userData[0].workoutsPerWeek);
        // if (userData[0].cardioPerWeek != null)
        //   setState(() => cardioPerWeek = userData[0].cardioPerWeek);
      }
    }

    if (hasLoadedOnce == false) {
      getUserData();
      hasLoadedOnce = true;
    }

    int _getSelectedPlanIndex() {
      if (goal == 'Endurance') return 0;
      if (goal == 'Hypertrophy') return 1;
      if (goal == 'Strength') return 2;
      if (goal == 'Pyramid') return 3;
      if (goal == 'Push Pull Legs') return 5;
      return 0;
    }

    Color sliderColor = Colors.black;

    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              padding: EdgeInsets.all(15),
              child: Column(
                children: <Widget>[
                  MarginBottom(),
                  (database.getUsername != null && database.getAvatar != null)
                      ? ClipRRect(
                          borderRadius: BorderRadius.circular(300.0),
                          child: Image(image: NetworkImage(database.getAvatar)),
                        )
                      : SizedBox(
                          height: 0,
                          width: 0,
                        ),
                  MarginBottom(),
                  (database.getUsername != null)
                      ? Text(
                          'Hello, ${database.getUsername}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      : Text('Hello!'),
                  MarginBottom(),
                  DropdownButton<String>(
                    value: genderDropdown,
                    icon: const Icon(Icons.arrow_downward),
                    iconSize: 12,
                    isExpanded: true,
                    elevation: 16,
                    onChanged: (String newValue) {
                      setState(() {
                        genderDropdown = newValue;
                      });
                    },
                    items: <String>['Male', 'Female', 'N/A']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                  // ## goal dropdown
                  MarginBottom(),
                  MarginBottom(),
                  Text(
                    // 'Pick a plan => ${_setGoal(goal)}',
                    'Pick a Plan',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  MarginBottom(gap: 8),

                  CarouselSlider(
                    options: CarouselOptions(
                      height: 350.0,
                      enableInfiniteScroll: false,
                      enlargeCenterPage: true,
                      initialPage: _getSelectedPlanIndex(),
                    ),
                    items: [
                      'Endurance',
                      'Hypertrophy',
                      'Strength',
                      'Pyramid',
                      'Push Pull Legs'
                    ].map((val) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                            child: PlanCard(
                                val: val,
                                goal: goal,
                                callback: (newValue) {
                                  setState(() => goal = newValue);
                                }),
                          );
                        },
                      );
                    }).toList(),
                  ),

                  MarginBottom(),
                  // MarginBottom(),
                  // DropdownButton<String>(
                  //   value: goal,
                  //   icon: const Icon(Icons.arrow_downward),
                  //   iconSize: 12,
                  //   isExpanded: true,
                  //   elevation: 16,
                  //   onChanged: (String newValue) {
                  //     setState(() {
                  //       goal = newValue;
                  //     });
                  //   },
                  //   items: <String>['Endurance', 'Hypertrophy', 'Strength']
                  //       .map<DropdownMenuItem<String>>((String value) {
                  //     return DropdownMenuItem<String>(
                  //       value: value,
                  //       child: Text(value),
                  //     );
                  //   }).toList(),
                  // ),
                  // ## Age Slider
                  MarginBottom(),
                  Text(
                    'Your Age: ${ageSlider.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: ageSlider,
                    min: 18,
                    max: 100,
                    divisions: 100,
                    label: ageSlider.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        ageSlider = value;
                      });
                    },
                  ),
                  // ## Weight Slider
                  MarginBottom(),
                  Text(
                    'Your Weight: ${weightSlider.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: weightSlider,
                    min: 0,
                    max: 300,
                    divisions: 205,
                    label: weightSlider.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        weightSlider = value;
                      });
                    },
                  ),
                  // ## Goal dropdown
                  // ## Days Per Week Slider
                  // MarginBottom(),
                  // Text(
                  //   'Lifting Days Per Week: ${workoutsPerWeek.round()}',
                  //   style: TextStyle(
                  //     fontWeight: FontWeight.bold,
                  //   ),
                  // ),
                  // Slider(
                  //   activeColor: sliderColor,
                  //   value: workoutsPerWeek,
                  //   min: 1,
                  //   max: 7,
                  //   divisions: 7,
                  //   label: workoutsPerWeek.round().toString(),
                  //   onChanged: (double value) {
                  //     setState(() {
                  //       workoutsPerWeek = value;
                  //     });
                  //   },
                  // ),
                  // ## Cardio Per Week Slider
                  // MarginBottom(),
                  // Text(
                  //   'Cardio Days Per Week: ${cardioPerWeek.round()}',
                  //   style: TextStyle(
                  //     fontWeight: FontWeight.bold,
                  //   ),
                  // ),
                  // Slider(
                  //   activeColor: sliderColor,
                  //   value: cardioPerWeek,
                  //   min: 1,
                  //   max: 7,
                  //   divisions: 7,
                  //   label: cardioPerWeek.round().toString(),
                  //   onChanged: (double value) {
                  //     setState(() {
                  //       cardioPerWeek = value;
                  //     });
                  //   },
                  // ),

                  // ## squat ORM
                  MarginBottom(),
                  Text(
                    'Squat One Rep Max (Estimate if unsure): ${squatORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: squatORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: squatORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        squatORM = value;
                      });
                    },
                  ),
                  // ## bench ORM
                  MarginBottom(),
                  Text(
                    'Bench One Rep Max: ${benchORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: benchORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: benchORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        benchORM = value;
                      });
                    },
                  ),
                  // ## deadlift ORM
                  MarginBottom(),
                  Text(
                    'Deadlift One Rep Max: ${deadliftORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: deadliftORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: deadliftORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        deadliftORM = value;
                      });
                    },
                  ),
                  // ## calf raise ORM
                  MarginBottom(),
                  Text(
                    'Calf Raise One Rep Max: ${calfRaisesORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: calfRaisesORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: calfRaisesORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        calfRaisesORM = value;
                      });
                    },
                  ),
                  // ## row raise ORM
                  MarginBottom(),
                  Text(
                    'Row One Rep Max: ${rowORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: rowORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: rowORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        rowORM = value;
                      });
                    },
                  ),
                  // ## curl ORM
                  MarginBottom(),
                  Text(
                    'Curl One Rep Max: ${curlORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: curlORM,
                    min: 5,
                    max: 150,
                    divisions: 145,
                    label: curlORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        curlORM = value;
                      });
                    },
                  ),
                  // ## uc ORM
                  MarginBottom(),
                  Text(
                    'Uppercut One Rep Max: ${uppercutORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: uppercutORM,
                    min: 5,
                    max: 200,
                    divisions: 195,
                    label: uppercutORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        uppercutORM = value;
                      });
                    },
                  ),
                  // ## glute ORM
                  MarginBottom(),
                  Text(
                    'Glute Bridge One Rep Max: ${gluteBridgeORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: gluteBridgeORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: gluteBridgeORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        gluteBridgeORM = value;
                      });
                    },
                  ),
                  // ## press ORM
                  MarginBottom(),
                  Text(
                    'Shoulder Press One Rep Max: ${pressORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: pressORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: pressORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        pressORM = value;
                      });
                    },
                  ),
                  // ## close grip ORM
                  MarginBottom(),
                  Text(
                    'Close Grip Bench One Rep Max: ${closeORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: closeORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: closeORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        closeORM = value;
                      });
                    },
                  ),
                  // ## romanian dead lift ORM
                  MarginBottom(),
                  Text(
                    'Romanian Deadlift One Rep Max: ${rdlORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: rdlORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: rdlORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        rdlORM = value;
                      });
                    },
                  ),
                  // ## glute ORM
                  MarginBottom(),
                  Text(
                    'Incline Bench One Rep Max: ${inclineORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: inclineORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: inclineORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        inclineORM = value;
                      });
                    },
                  ),
                  // ## lat raises ORM
                  MarginBottom(),
                  Text(
                    'Lat Raises One Rep Max: ${latORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: latORM,
                    min: 5,
                    max: 100,
                    divisions: 25,
                    label: latORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        latORM = value;
                      });
                    },
                  ),
                  // ## shrugs ORM
                  MarginBottom(),
                  Text(
                    'Shrug One Rep Max: ${shrugORM.round()}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Slider(
                    activeColor: sliderColor,
                    value: shrugORM,
                    min: 45,
                    max: 400,
                    divisions: 355,
                    label: shrugORM.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        shrugORM = value;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          //  ## Submit button
          Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.4),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 0), // changes position of shadow
                ),
              ],
            ),
            child: SubmitButton(
              title: 'Save Profile',
              bg: Colors.pinkAccent[400],
              onPressed: () async {
                dynamic workoutData = await _readJson(goal);

                await showDialog(
                  context: context,
                  builder: (context) => new AlertDialog(
                    title: new Text('Hold up...'),
                    content: Text(
                        'Changing your profile will reset any workout data you have saved. Are you sure you want switch plans?'),
                    actions: <Widget>[
                      new SubmitButton(
                        onPressed: () async {
                          try {
                            await database.setUserData(UserDataModel(
                              gender: genderDropdown,
                              age: ageSlider,
                              weight: weightSlider,
                              squatORM: squatORM,
                              benchORM: benchORM,
                              pressORM: pressORM,
                              closeORM: closeORM,
                              rdlORM: rdlORM,
                              inclineORM: inclineORM,
                              latORM: latORM,
                              shrugORM: shrugORM,
                              deadliftORM: deadliftORM,
                              calfRaisesORM: calfRaisesORM,
                              rowORM: rowORM,
                              curlORM: curlORM,
                              uppercutORM: uppercutORM,
                              gluteBridgeORM: gluteBridgeORM,
                              goal: goal,
                              workoutData: workoutData,
                              // workoutsPerWeek: workoutsPerWeek,
                              // cardioPerWeek: cardioPerWeek,
                            ));

                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text('Profile Updated'),
                                duration: Duration(seconds: 3),
                              ),
                            );
                          } catch (e) {
                            PlatformExceptionAlert(
                                    title: 'Uh oh, couldn\'t update profile',
                                    exception: e)
                                .show(context);
                          }

                          Navigator.of(context, rootNavigator: true)
                              .pop(); // dismisses only the dialog and returns nothing
                        },
                        title: 'OK',
                        bg: Colors.black,
                        text: Colors.white,
                      ),
                      new SubmitButton(
                        onPressed: () {
                          Navigator.of(context, rootNavigator: true).pop(
                              false); // dismisses only the dialog and returns false
                        },
                        title: 'Cancel',
                        bg: Colors.white,
                        text: Colors.black,
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
