import 'package:flutter/material.dart';
import 'package:nerd_fit/utils/getAuth.dart';
import 'package:provider/provider.dart';

import 'package:nerd_fit/providers/auth.dart';
import 'package:nerd_fit/providers/database.dart';

import 'package:nerd_fit/app/signin/SignInScreen.dart';
import 'package:nerd_fit/screens/HomeScreen.dart';
import 'package:nerd_fit/components/common/LoadingIndicator.dart';

class LandingPageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final auth = getAuth(context);

    return StreamBuilder<User>(
      stream: auth.onAuthStateChanged,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          User user = snapshot.data;
          // if  no user, send to auth flow
          if (user == null) return SignInScreen.create(context);

          // if there already is a user, show them the homepage
          return Provider<Database>(
            child: HomeScreen(),
            create: (_) => FirestoreDatabase(
              uid: user.uid,
              username: user.username,
              email: user.email,
              avatar: user.avatar,
            ),
          );
        } else {
          return LodingIndicator(true);
        }
      },
    );
  }
}
