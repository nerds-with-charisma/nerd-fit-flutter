import 'package:flutter/material.dart';
import 'package:nerd_fit/app/home/TabItem.dart';
import 'package:nerd_fit/screens/HomeScaffold.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TabItem _currentTab = TabItem.shoppingList;

  @override
  Widget build(BuildContext context) {
    return HomeScaffold(
      currentTab: _currentTab,
      onSelectTab: (item) {
        setState(() => _currentTab = item);
        // pop if we  can, else don't pop (if it's from the app drawer, we'll pop to close it)
        Navigator.of(context).maybePop();
      },
    );
  }
}
