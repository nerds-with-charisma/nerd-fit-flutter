import 'package:flutter/material.dart';
import 'package:nerd_fit/app/habits/HabitListView.dart';
import 'package:nerd_fit/app/home/TabItem.dart';

class HabitScreen extends StatelessWidget {
  HabitScreen({this.onSelectTab});

  final ValueChanged<TabItem> onSelectTab;

  static const routeName = '/habit';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg--shopping.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 8,
                offset: const Offset(0, 0),
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              HabitListView(onSelectTab: onSelectTab),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          onSelectTab(TabItem.editHabit);
        },
        child: const Icon(Icons.add),
        backgroundColor: Colors.black87,
      ),
    );
  }
}
