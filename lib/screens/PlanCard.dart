import 'package:flutter/material.dart';

class PlanCard extends StatelessWidget {
  PlanCard({this.val, this.goal, this.callback});
  final String val;
  final Function callback;
  final String goal;

  dynamic _setData() {
    switch (val.toLowerCase()) {
      case 'endurance':
        return {
          'title': 'Basic Endurance Workout',
          'desc':
              'Focus on compound lifts mixed with some isolation exercises. Typically in the 12 rep range. Perfect for those just starting out.',
          'img':
              'https://images.unsplash.com/photo-1574680096145-d05b474e2155?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=340&h=230&q=80',
        };
        break;
      case 'hypertrophy':
        return {
          'title': 'Hypertrophy Workout',
          'desc':
              'Gain muscle and lose fat. A nice step up from the endurance plan where you\'ll have lower reps but heavier weights.',
          'img':
              'https://images.unsplash.com/photo-1591804671002-b24e17464f9b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=340&h=230&q=80'
        };
        break;
      case 'pyramid':
        return {
          'title': 'Pyramid Workout',
          'desc':
              'Start low & build your way up then go back down. This routine will if you might be lifting too heavy. We also up the cardio game.',
          'img':
              'https://images.unsplash.com/photo-1526404079162-d93dafdeef3e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=340&h=230&q=80'
        };
        break;
      case 'push pull legs':
        return {
          'title': 'Push Pull Legs',
          'desc':
              'The classic PPL routine, 5 days a week. Mix in a decent bit of cardio.',
          'img':
              'https://images.unsplash.com/photo-1550345332-09e3ac987658?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=340&h=230&q=80'
        };
        break;
      case 'strength':
        return {
          'title': 'Stength Stronglifts Variation',
          'desc':
              'A stronglifts 5x5 program where you will be lifting heavy for 5 sets of 5 reps. To get big, you need to lift heavy.',
          'img':
              'https://images.unsplash.com/photo-1602489346918-6e0a2afa8133?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=340&h=230&q=80'
        };
        break;
      default:
        return {
          'title': 'Error Loading Workout',
          'desc': '',
          'img':
              'https://images.unsplash.com/photo-1603233720024-4ee0592a58f9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=340&h=230&q=80',
        };
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipRect(
      child: Card(
        elevation: (goal == val) ? 4 : 2,
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.arrow_drop_down_circle),
              title: Text(_setData()['title']),
              subtitle: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: Text(
                  _setData()['desc'],
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.6),
                    fontSize: 12,
                  ),
                ),
              ),
            ),
            Image.network(_setData()['img']),
            ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 100,
                  child: TextButton(
                    child: Text(
                      (goal == val) ? 'Current Plan' : 'Select Plan',
                      style: TextStyle(
                        color: (goal == val)
                            ? Colors.pinkAccent[400]
                            : Colors.black,
                      ),
                    ),
                    onPressed: () => callback(val),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
