import 'package:flutter/material.dart';
import 'package:nerd_fit/app/home/TabItem.dart';

import 'package:nerd_fit/app/workout/NoWorkouts.dart';
import 'package:nerd_fit/app/workout/WorkoutProgressCard.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class WorkoutProgressScreen extends StatefulWidget {
  WorkoutProgressScreen({this.onSelectTab});
  final ValueChanged<TabItem> onSelectTab;

  static const routeName = '/progress';

  @override
  State<WorkoutProgressScreen> createState() => _WorkoutProgressScreenState();
}

class _WorkoutProgressScreenState extends State<WorkoutProgressScreen> {
  final workouts = [];
  dynamic userData = null;
  int currentWorkoutIndex = 0;
  List _items = [];
  bool hasLoaded = false;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);

    Future<void> _readJson() async {
      // get user info
      final userProfileData = await database.getUserDataStream().first;
      setState(() => userData = userProfileData);

      // set json to state
      setState(() => _items = userData[0].workoutData['workouts']);

      // variables for finding current workout
      setState(() => currentWorkoutIndex =
          _items.indexWhere((w) => w['isActive'] == true));

      // don't forget to increment active after finishing
      print(_items[currentWorkoutIndex]);

      setState(() => hasLoaded = true);
    }

    if (hasLoaded == false) _readJson();

    return Scaffold(
      body: isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.grey[100],
              child: (userData == null ||
                      userData[0] == null ||
                      _items.length == 0)
                  ? NoWorkouts(
                      onSelectTab: widget.onSelectTab,
                    )
                  : Container(
                      width: double.infinity,
                      height: double.infinity,
                      child: SingleChildScrollView(
                        child: Center(
                          child: new Column(
                              children: _items.map(
                            (exercise) {
                              var index = _items.indexOf(exercise);
                              return WorkoutProgressCard(
                                currentWorkoutIndex: currentWorkoutIndex,
                                index: index,
                                exercise: exercise,
                              );
                            },
                          ).toList()),
                        ),
                      ),
                    ),
            ),
    );
  }
}
