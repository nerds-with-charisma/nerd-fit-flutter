import 'package:flutter/material.dart';
import 'package:nerd_fit/app/home/TabItem.dart';
import 'package:nerd_fit/app/workout/CurrentWorkout.dart';

import 'package:nerd_fit/app/workout/NoWorkouts.dart';
import 'package:nerd_fit/components/PlatformExceptionAlert.dart';
import 'package:nerd_fit/components/SubmitButton.dart';
import 'package:nerd_fit/models/UserDataModel.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class WorkoutScreen extends StatefulWidget {
  WorkoutScreen({this.onSelectTab});
  final ValueChanged<TabItem> onSelectTab;

  static const routeName = '/workouts';

  @override
  State<WorkoutScreen> createState() => _WorkoutScreenState();
}

class _WorkoutScreenState extends State<WorkoutScreen> {
  final workouts = [];
  dynamic userData = null;
  int currentWorkoutIndex = 0;
  List _items = [];
  bool hasLoaded = false;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);

    Future<void> _completeWorkout() async {
      setState(() {
        isLoading = true;
      });

      print(userData);
      dynamic userDataMute = userData;

      int totalWorkoutCount = userDataMute[0].workoutData['workouts'].length;

      // if last workout, send  back to start and alert that they're done
      if (currentWorkoutIndex + 1 == totalWorkoutCount) {
        await showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  title: new Text('You did it!!!'),
                  content: Text(
                      'You finished the current plan. You can start this one again, or you can go to your profile and switch to the next level.'),
                  actions: <Widget>[
                    new SubmitButton(
                      onPressed: () {
                        userDataMute[0].workoutData['workouts'][0]['isActive'] =
                            true;

                        Navigator.of(context, rootNavigator: true).pop(
                            false); // dismisses only the dialog and returns false
                      },
                      title: 'Start this plan over',
                    ),
                    new SubmitButton(
                      onPressed: () async {
                        widget.onSelectTab(TabItem.profile);
                        Navigator.of(context, rootNavigator: true)
                            .pop(); // dismisses only the dialog and returns nothing
                      },
                      title: 'Pick a new plan',
                    ),
                  ],
                ));
      } else {
        // unset the old workout
        userDataMute[0].workoutData['workouts'][currentWorkoutIndex]
            ['isActive'] = false;

        // set the next one to active
        userDataMute[0].workoutData['workouts'][currentWorkoutIndex + 1]
            ['isActive'] = true;
      }

      // update visually
      //setState(() => _items = userDataMute[0].workoutData['workouts']);

      // increment orm's where applicable

      // update in db
      try {
        await database.setUserData(UserDataModel(
          gender: userData[0].gender,
          age: userData[0].age,
          weight: userData[0].weight,
          squatORM: userData[0].squatORM,
          benchORM: userData[0].benchORM,
          deadliftORM: userData[0].deadliftORM,
          calfRaisesORM: userData[0].calfRaisesORM,
          rowORM: userData[0].rowORM,
          curlORM: userData[0].curlORM,
          uppercutORM: userData[0].uppercutORM,
          gluteBridgeORM: userData[0].gluteBridgeORM,
          pressORM: userData[0].pressORM,
          closeORM: userData[0].closeORM,
          rdlORM: userData[0].rdlORM,
          inclineORM: userData[0].inclineORM,
          latORM: userData[0].latORM,
          shrugORM: userData[0].shrugORM,
          goal: userData[0].goal,
          workoutData: userData[0].workoutData,
          // workoutsPerWeek: workoutsPerWeek,
          // cardioPerWeek: cardioPerWeek,
        ));

        setState(() {
          isLoading = false;
        });

        setState(() => currentWorkoutIndex =
            _items.indexWhere((w) => w['isActive'] == true));

        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Workout Completed'),
            duration: Duration(seconds: 3),
          ),
        );
      } catch (e) {
        PlatformExceptionAlert(
                title: 'Our bad, we couldn\'t connect to the server.',
                exception: e)
            .show(context);
      }
    }

    ;

    Future<void> _readJson() async {
      print('reloading');
      // get user info
      final userProfileData = await database.getUserDataStream().first;
      setState(() => userData = userProfileData);

      // set json to state
      if (userData.length == 0 || !userData[0]) {
        return false;
      }
      setState(() => _items = userData[0].workoutData['workouts']);

      // variables for finding current workout
      setState(() => currentWorkoutIndex =
          _items.indexWhere((w) => w['isActive'] == true));

      // don't forget to increment active after finishing
      print(_items[currentWorkoutIndex]);

      setState(() => hasLoaded = true);

      return;
    }

    if (hasLoaded == false) _readJson();

    return Scaffold(
      body: isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/bg--shopping.png'),
                  fit: BoxFit.cover,
                ),
              ),
              width: double.infinity,
              height: double.infinity,
              child: (userData == null ||
                      userData.length == 0 ||
                      userData[0] == null ||
                      _items.length == 0)
                  ? NoWorkouts()
                  : CurrentWorkout(
                      userData: userData[0],
                      item: _items[currentWorkoutIndex],
                      completeWorkout: _completeWorkout,
                      reload: _readJson,
                    ),
            ),
    );
  }
}
