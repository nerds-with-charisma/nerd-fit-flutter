import 'package:flutter/material.dart';
import 'package:nerd_fit/app/home/TabItem.dart';
import 'package:nerd_fit/components/MainAppBar.dart';
import 'package:nerd_fit/components/navigation/AppDrawer.dart';
import 'package:nerd_fit/components/shoppingList/handleShoppingListDropdown.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:nerd_fit/utils/getInitialTab.dart';
import 'package:nerd_fit/utils/setRoute.dart';

class HomeScaffold extends StatefulWidget {
  HomeScaffold({@required this.currentTab, @required this.onSelectTab});

  final TabItem currentTab;
  final ValueChanged<TabItem> onSelectTab;

  @override
  State<HomeScaffold> createState() => _HomeScaffoldState();
}

class _HomeScaffoldState extends State<HomeScaffold> {
  String title = '';
  List<Widget> actions = [];
  bool filter = false;
  String sort = 'title';
  bool descending = false;

  // ask for push notifications :: android
  @override
  void initState() {
    super.initState();
    final fbm = FirebaseMessaging();
    fbm.requestNotificationPermissions();

    getInitialTab(widget.onSelectTab);
  }

  // handle dropdown change for shopping list
  void handleDrop(val) {
    dynamic sortingData =
        handleShoppingListDropdown(val, filter, sort, descending);

    setState(() {
      sort = sortingData['sort'];
      descending = sortingData['descending'];
      filter = sortingData['filter'];
    });
  }

  Widget get widgetBuilders {
    // setting up our routes
    dynamic routes = setRoute(widget.currentTab, filter, sort, handleDrop,
        descending, widget.onSelectTab);

    setState(() {
      title = routes['title'];
      actions = routes['actions'];
    });

    return routes['component'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      drawer: AppDrawer(
        currentTab: widget.currentTab,
        onSelectTab: widget.onSelectTab,
      ),
      body: widgetBuilders,
      appBar: mainAppBar(title: title, actions: actions),
    );
  }
}
