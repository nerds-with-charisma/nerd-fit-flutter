import 'package:flutter/material.dart';
import 'package:nerd_fit/app/dimensions/dimensionCard.dart';

class BodyDimensions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/bg--shopping.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            DimensionCard(title: 'Weight'),
            DimensionCard(title: 'Neck'),
            DimensionCard(title: 'Chest'),
            DimensionCard(title: 'Bicep'),
            DimensionCard(title: 'Waist'),
            DimensionCard(title: 'Thigh'),
            //DimensionCard(title: 'Belly Button'),
            //DimensionCard(title: 'Love Handle'),
          ],
        ),
      ),
    );
  }
}
