import 'package:flutter/material.dart';

import 'package:nerd_fit/app/signin/EmailSignInFormBlockBased.dart';
import 'package:nerd_fit/utils/statics.dart';

class EmailLoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nerd Fit Login'),
        centerTitle: true,
        elevation: 8.0,
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(statics()['standardBackground']),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            child: SigninFormBlocBased.create(context),
          ),
          padding: const EdgeInsets.all(5.0),
        ),
      ),
    );
  }
}
