import 'package:flutter/foundation.dart';

class HabitModel with ChangeNotifier {
  final String id;
  final String title;
  final String occurance;
  final String createdAt;
  final String description;
  final bool complete;
  final int streak;

  HabitModel({
    @required this.id,
    @required this.title,
    @required this.occurance,
    @required this.createdAt,
    @required this.description,
    @required this.complete,
    this.streak,
  });

  factory HabitModel.fromMap(Map<String, dynamic> data, String documentId) {
    if (data == null) return null;
    final String title = data['title'];
    final String occurance = data['occurance'];
    final String createdAt = data['createdAt'];
    final String description = data['description'];
    final bool complete = data['complete'];
    final int streak = data['streak'];

    return HabitModel(
      id: documentId,
      title: title,
      occurance: occurance,
      createdAt: createdAt,
      description: description,
      complete: complete,
      streak: streak,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'occurance': occurance,
      'createdAt': createdAt,
      'description': description,
      'complete': complete,
      'streak': streak,
    };
  }
}
