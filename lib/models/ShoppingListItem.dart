import 'package:flutter/foundation.dart';

class ShoppingListItem with ChangeNotifier {
  final String id;
  final String title;
  final String date;
  bool isNeeded;

  ShoppingListItem({
    @required this.id,
    @required this.title,
    this.date,
    this.isNeeded = false,
  });

  factory ShoppingListItem.fromMap(
      Map<String, dynamic> data, String documentId) {
    if (data == null) return null;
    final String title = data['title'];
    final String date = data['date'];
    final bool isNeeded = data['isNeeded'];

    return ShoppingListItem(
      id: documentId,
      title: title,
      isNeeded: isNeeded,
      date: date,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'isNeeded': isNeeded,
      'date': date,
    };
  }
}
