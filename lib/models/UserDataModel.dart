import 'package:flutter/foundation.dart';

class UserDataModel with ChangeNotifier {
  final String gender;
  final age;
  final weight;
  final originalWeight;
  final workoutsPerWeek;
  final cardioPerWeek;
  final String goal;
  final squatORM;

  final benchORM;
  final deadliftORM;
  final calfRaisesORM;
  final rowORM;
  final curlORM;
  final uppercutORM;
  final gluteBridgeORM;

  final pressORM;
  final closeORM;
  final rdlORM;
  final inclineORM;
  final latORM;
  final shrugORM;
  final dynamic workoutData;
  final dimsWeight;
  final dimsNeck;
  final dimsChest;
  final dimsBicep;
  final dimsWaist;
  final dimsThigh;
  final dimsBellyButton;
  final originalDimsWeight;
  final originalDimsNeck;
  final originalDimsChest;
  final originalDimsBicep;
  final originalDimsWaist;
  final originalDimsThigh;
  final originalDimsBellyButton;

  UserDataModel({
    this.gender,
    this.age,
    this.weight,
    this.originalWeight,
    this.workoutsPerWeek,
    this.cardioPerWeek,
    this.goal,
    this.squatORM,
    this.benchORM,
    this.deadliftORM,
    this.calfRaisesORM,
    this.rowORM,
    this.curlORM,
    this.uppercutORM,
    this.gluteBridgeORM,
    this.pressORM,
    this.closeORM,
    this.rdlORM,
    this.inclineORM,
    this.latORM,
    this.shrugORM,
    this.workoutData,
    this.dimsWeight,
    this.dimsNeck,
    this.dimsChest,
    this.dimsBicep,
    this.dimsWaist,
    this.dimsThigh,
    this.dimsBellyButton,
    this.originalDimsWeight,
    this.originalDimsNeck,
    this.originalDimsChest,
    this.originalDimsBicep,
    this.originalDimsWaist,
    this.originalDimsThigh,
    this.originalDimsBellyButton,
  });

  factory UserDataModel.fromMap(Map<String, dynamic> data, String documentId) {
    if (data == null) return null;

    final String gender = data['gender'];
    final double age = data['age'];
    final weight = data['weight'];
    final originalWeight = data['originalWeight'];
    final String goal = data['goal'];
    // final double workoutsPerWeek = data['workoutsPerWeek'];
    // final double cardioPerWeek = data['cardioPerWeek'];

    final squatORM = data['squatORM'];
    final benchORM = data['benchORM'];
    final deadliftORM = data['deadliftORM'];
    final calfRaisesORM = data['calfRaisesORM'];
    final rowORM = data['rowORM'];
    final curlORM = data['curlORM'];
    final uppercutORM = data['uppercutORM'];
    final gluteBridgeORM = data['gluteBridgeORM'];

    final pressORM = data['pressORM'];
    final closeORM = data['closeORM'];
    final rdlORM = data['rdlORM'];
    final inclineORM = data['inclineORM'];
    final latORM = data['latORM'];
    final shrugORM = data['shrugORM'];
    final dynamic workoutData = data['workoutData'];

    final dimsWeight = data['dimsWeight'];
    final dimsNeck = data['dimsNeck'];
    final dimsChest = data['dimsChest'];
    final dimsBicep = data['dimsBicep'];
    final dimsWaist = data['dimsWaist'];
    final dimsThigh = data['dimsThigh'];
    final dimsBellyButton = data['dimsBellyButton'];
    final originalDimsWeight = data['originalDimsWeight'];
    final originalDimsNeck = data['originalDimsNeck'];
    final originalDimsChest = data['originalDimsChest'];
    final originalDimsBicep = data['originalDimsBicep'];
    final originalDimsWaist = data['originalDimsWaist'];
    final originalDimsThigh = data['originalDimsThigh'];
    final originalDimsBellyButton = data['originalDimsBellyButton'];

    return UserDataModel(
      gender: gender,
      age: age,
      weight: weight,
      originalWeight: originalWeight,
      goal: goal,
      // workoutsPerWeek: workoutsPerWeek,
      // cardioPerWeek: cardioPerWeek,
      squatORM: squatORM.roundToDouble(),
      benchORM: benchORM.roundToDouble(),
      deadliftORM: deadliftORM.roundToDouble(),
      calfRaisesORM: calfRaisesORM.roundToDouble(),
      rowORM: rowORM.roundToDouble(),
      curlORM: curlORM.roundToDouble(),
      uppercutORM: uppercutORM.roundToDouble(),
      gluteBridgeORM: gluteBridgeORM.roundToDouble(),
      pressORM: pressORM,
      closeORM: closeORM,
      rdlORM: rdlORM,
      inclineORM: inclineORM,
      latORM: latORM,
      shrugORM: shrugORM,
      workoutData: workoutData,
      dimsWeight: dimsWeight,
      dimsNeck: dimsNeck,
      dimsChest: dimsChest,
      dimsBicep: dimsBicep,
      dimsWaist: dimsWaist,
      dimsThigh: dimsThigh,
      dimsBellyButton: dimsBellyButton,
      originalDimsWeight: originalDimsWeight,
      originalDimsNeck: originalDimsNeck,
      originalDimsChest: originalDimsChest,
      originalDimsBicep: originalDimsBicep,
      originalDimsWaist: originalDimsWaist,
      originalDimsThigh: originalDimsThigh,
      originalDimsBellyButton: originalDimsBellyButton,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'gender': gender,
      'age': age,
      'weight': weight,
      'originalWeight': originalWeight,
      'goal': goal,
      // 'workoutsPerWeek': workoutsPerWeek,
      // 'cardioPerWeek': cardioPerWeek,
      'squatORM': squatORM,
      'benchORM': benchORM,
      'deadliftORM': deadliftORM,
      'calfRaisesORM': calfRaisesORM,
      'rowORM': rowORM,
      'curlORM': curlORM,
      'uppercutORM': uppercutORM,
      'gluteBridgeORM': gluteBridgeORM,
      'pressORM': pressORM,
      'closeORM': closeORM,
      'rdlORM': rdlORM,
      'inclineORM': inclineORM,
      'latORM': latORM,
      'shrugORM': shrugORM,
      'workoutData': workoutData,
      'dimsWeight': dimsWeight,
      'dimsNeck': dimsNeck,
      'dimsChest': dimsChest,
      'dimsBicep': dimsBicep,
      'dimsWaist': dimsWaist,
      'dimsThigh': dimsThigh,
      'dimsBellyButton': dimsBellyButton,
      'originalDimsWeight': originalDimsWeight,
      'originalDimsNeck': originalDimsNeck,
      'originalDimsChest': originalDimsChest,
      'originalDimsBicep': originalDimsBicep,
      'originalDimsWaist': originalDimsWaist,
      'originalDimsThigh': originalDimsThigh,
      'originalDimsBellyButton': originalDimsBellyButton,
    };
  }
}
