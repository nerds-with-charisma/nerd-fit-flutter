// get previously shared data
import 'package:nerd_fit/providers/auth.dart';
import 'package:provider/provider.dart';

getAuth(context) {
  final auth = Provider.of<AuthBase>(context);
  return auth;
}
