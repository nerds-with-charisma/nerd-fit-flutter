import 'package:nerd_fit/app/home/TabItem.dart';
import 'package:nerd_fit/utils/setSharedPref.dart';

void setStartingScreen(TabItem tabItem, Function onSelectTab) {
  onSelectTab(tabItem);
  setSharedPref('startingTab', tabItem.toString());
}
