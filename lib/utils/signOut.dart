import 'package:flutter/material.dart';
import 'package:nerd_fit/providers/auth.dart';
import 'package:provider/provider.dart';

Future<void> signOut(BuildContext context) async {
  final auth = Provider.of<AuthBase>(context, listen: false);

  try {
    await auth.signOut();
  } catch (e) {
    print(e.toString());
  }
}
