import 'package:nerd_fit/app/habits/EditHabit.dart';
import 'package:nerd_fit/app/home/TabItem.dart';
import 'package:nerd_fit/components/ShoppingListSortDropdown.dart';
import 'package:nerd_fit/components/WorkoutActions.dart';
import 'package:nerd_fit/screens/BodyDimensions.dart';
import 'package:nerd_fit/screens/HabitScreen.dart';
import 'package:nerd_fit/screens/ProfileScreen.dart';
import 'package:nerd_fit/screens/ShoppingListScreen.dart';
import 'package:nerd_fit/screens/TipsScreen.dart';
import 'package:nerd_fit/screens/WorkoutProgressScreen.dart';
import 'package:nerd_fit/screens/WorkoutScreen.dart';

setRoute(TabItem currentTab, bool filter, String sort, Function handleDrop,
    bool descending, Function onSelectTab) {
  dynamic routeData = {'title': null, 'actions': null, 'filter': filter};

  if (currentTab == TabItem.shoppingList) {
    routeData = {
      'title': null,
      'actions': [
        ShoppingListSortDropdown(
          filter: filter,
          sort: sort,
          callback: (val) => handleDrop(val),
        )
      ],
      'component': ShoppingListScreen(
        filter: filter,
        sort: sort,
        descending: descending,
      ),
    };
  } else if (currentTab == TabItem.habits) {
    routeData = {
      'title': 'Your Habits',
      'actions': null,
      'component': HabitScreen(
        onSelectTab: onSelectTab,
      ),
    };
  } else if (currentTab == TabItem.editHabit) {
    routeData = {
      'title': 'Edit Habit',
      'actions': null,
      'component': EditHabit(
        onSelectTab: onSelectTab,
      ),
    };
  } else if (currentTab == TabItem.profile) {
    routeData = {
      'title': 'Your Profile',
      'actions': null,
      'component': ProfileScreen(),
    };
  } else if (currentTab == TabItem.workouts) {
    routeData = {
      'title': 'Today\'s Workout',
      'actions': [
        WorkoutActions(),
      ],
      'component': WorkoutScreen(
        onSelectTab: onSelectTab,
      ),
    };
  } else if (currentTab == TabItem.progress) {
    routeData = {
      'title': 'Workout Schedule',
      'actions': null,
      'component': WorkoutProgressScreen(
        onSelectTab: onSelectTab,
      ),
    };
  } else if (currentTab == TabItem.tips) {
    routeData = {
      'title': 'Fitness Tips',
      'actions': null,
      'component': TipsScreen(),
    };
  } else if (currentTab == TabItem.dimensions) {
    routeData = {
      'title': 'Log Body Dimensions',
      'actions': null,
      'component': BodyDimensions(),
    };
  } else {
    routeData = {
      'title': null,
      'actions': null,
      'component': ShoppingListScreen(
        filter: filter,
        sort: sort,
        descending: descending,
      ),
    };
  }

  return routeData;
}
