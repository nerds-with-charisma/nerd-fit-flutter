// get previously shared data

import 'package:flutter/material.dart';
import 'package:nerd_fit/components/PlatformExceptionAlert.dart';
import 'package:nerd_fit/components/platform_alert_dialoge.dart';
import 'package:nerd_fit/models/ShoppingListItem.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:nerd_fit/utils/getDb.dart';

createShoppingListItem(item, title, BuildContext context) async {
  final database = await getDb(context); // connect to db

  // get current items from FB
  final existingItems = await database.shoppingListItemStream().first;
  final allTitles = existingItems.map((item) => item.title).toList();
  final id = item?.id ?? documentIdFromCurrentDate();

  if (allTitles.contains(title)) {
    PlatformAlertDialog(
      title: 'Item is already in your list',
      content: 'Please add an item not already on your shopping list.',
      defaultActionText: 'OK',
    ).show(context);

    return false;
  } else {
    try {
      await database.setShoppingListItem(ShoppingListItem(
        id: id,
        title: title[0].toUpperCase() + title.substring(1),
        isNeeded: false,
        date: DateTime.now().toIso8601String(),
      ));
    } catch (e) {
      PlatformExceptionAlert(title: 'Operation  failed', exception: e)
          .show(context);
    }

    return;
  }
}
