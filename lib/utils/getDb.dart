// get previously shared data
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

getDb(context) {
  final db = Provider.of<Database>(context, listen: false);
  return db;
}
