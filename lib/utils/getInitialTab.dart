import 'package:nerd_fit/app/home/TabItem.dart';
import 'package:nerd_fit/utils/getSharedPref.dart';

getInitialTab(Function onSelectedTab) async {
  // check shared prefs too, to load right tab
  String startingTab = await getSharedPref('startingTab');
  if (startingTab != null) {
    if (startingTab == 'TabItem.habits') onSelectedTab(TabItem.habits);
    if (startingTab == 'TabItem.shoppingList')
      onSelectedTab(TabItem.shoppingList);
    if (startingTab == 'TabItem.workouts') onSelectedTab(TabItem.workouts);
    if (startingTab == 'TabItem.tips') onSelectedTab(TabItem.tips);
  }
}
