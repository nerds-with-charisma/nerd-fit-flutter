statics() {
  return {
    'logoGradient': 'assets/images/logo--gradient.png',
    'standardBackground': 'assets/images/bg--login.png',
  };
}
