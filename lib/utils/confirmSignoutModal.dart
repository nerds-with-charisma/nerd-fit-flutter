import 'package:flutter/material.dart';
import 'package:nerd_fit/components/platform_alert_dialoge.dart';
import 'package:nerd_fit/utils/signOut.dart';

Future<void> confirmSignOut(BuildContext context) async {
  final didRequestSignOut = await PlatformAlertDialog(
    title: 'Logout',
    cancelAction: 'Cancel',
    content: 'Are you sure you want to logout?',
    defaultActionText: 'Logout',
  ).show(context);

  if (didRequestSignOut == true) signOut(context);
}
