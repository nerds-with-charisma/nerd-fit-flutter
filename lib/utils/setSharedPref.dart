import 'package:shared_preferences/shared_preferences.dart';

// set shared data
setSharedPref(name, val) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.setString(name, val);
}
