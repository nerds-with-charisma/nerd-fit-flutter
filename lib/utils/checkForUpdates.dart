import 'package:flutter/material.dart';
import 'package:nerd_fit/components/SubmitButton.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> checkForUpdates(BuildContext context) async {
  // :: todo - update version to check for new versions
  String newerVersion =
      'https://gitlab.com/nerds-with-charisma/nerd-fit-flutter/-/raw/master/app-release';

  if (await canLaunch('${newerVersion}.apk')) {
    // there is a new minor version to update
    launch('${newerVersion}.apk');
  } else {
    await showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('All good'),
              content: Text('You have the latest version already.'),
              actions: <Widget>[
                new SubmitButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop(
                        false); // dismisses only the dialog and returns false
                  },
                  title: 'Ok',
                ),
              ],
            ));
  }
}
