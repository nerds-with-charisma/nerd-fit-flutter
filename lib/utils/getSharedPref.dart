import 'package:shared_preferences/shared_preferences.dart';

// get previously shared data
getSharedPref(name) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String data = preferences.getString(name);

  return data;
}
