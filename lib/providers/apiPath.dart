class APIPath {
  // user info
  static String updateUserInfo(String uid, String userData) =>
      'users/$uid/userData/$userData';

  // all items
  static String getuserData(String uid) => 'users/$uid/userData';

  // individual item
  static String shoppingListItem(String uid, String itemId) =>
      'users/$uid/shoppingListItems/$itemId';

  // all items
  static String shoppingListItems(String uid) => 'users/$uid/shoppingListItems';

  // individual habit
  static String habit(String uid, String habitId) =>
      'users/$uid/habits/$habitId';

  // all habits
  static String habits(String uid) => 'users/$uid/habits';
}
