import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:nerd_fit/models/HabbitModel.dart';
import 'dart:async';

import 'package:nerd_fit/models/ShoppingListItem.dart';
import 'package:nerd_fit/models/UserDataModel.dart';
import 'package:nerd_fit/providers/apiPath.dart';

abstract class Database {
  // user
  String get getUsername;
  String get getAvatar;
  Future<void> setUserData(UserDataModel user);
  Future<void> updateUserData(data);
  Stream<List<UserDataModel>> getUserDataStream();

  // shopping
  Future<void> setShoppingListItem(ShoppingListItem item);
  Stream<List<ShoppingListItem>> shoppingListItemStream({sort, descending});
  Future<void> deleteShoppingListItem(ShoppingListItem item);

  //habit
  Future<void> setHabitListItem(HabitModel item);
  Future<void> deleteHabitListItem(HabitModel item);
  Stream<List<HabitModel>> habitListItemStream();
}

// helper for creating an id from the current timestamp
String documentIdFromCurrentDate() => DateTime.now().toIso8601String();

class FirestoreDatabase implements Database {
  FirestoreDatabase(
      {@required this.uid, this.username, this.email, this.avatar})
      : assert(uid != null);

  final String uid;
  final String username;
  final String email;
  final String avatar;

  // ## User  info
  String get getAvatar => avatar;
  String get getUsername => username;

  Future<void> setUserData(UserDataModel user) async => await _setData(
        path: APIPath.updateUserInfo(uid, 'userData'),
        data: user.toMap(),
      );

  // update specific user values
  Future<void> updateUserData(data) async {
    final reference =
        Firestore.instance.document(APIPath.updateUserInfo(uid, 'userData'));

    await reference.updateData(data);
  }

  // get all habits
  Stream<List<UserDataModel>> getUserDataStream() {
    return _collectionStream(
      path: APIPath.getuserData(uid),
      builder: (data, documentId) => UserDataModel.fromMap(data, documentId),
      sort: 'age',
      descending: false,
    );
  }

  // ## Shopping list
  // make a new shopping item
  Future<void> setShoppingListItem(ShoppingListItem item) async =>
      await _setData(
        path: APIPath.shoppingListItem(uid, item.id),
        data: item.toMap(),
      );

  // delete an item
  Future<void> deleteShoppingListItem(ShoppingListItem item) async =>
      await _deleteData(
        path: APIPath.shoppingListItem(uid, item.id),
      );

  // get all items
  Stream<List<ShoppingListItem>> shoppingListItemStream(
      {sort = 'title', descending = false}) {
    return _collectionStream(
      path: APIPath.shoppingListItems(uid),
      builder: (data, documentId) => ShoppingListItem.fromMap(data, documentId),
      sort: sort,
      descending: descending,
    );
  }

  // ## Habits
  // get all habits
  Stream<List<HabitModel>> habitListItemStream() {
    return _collectionStream(
      path: APIPath.habits(uid),
      builder: (data, documentId) => HabitModel.fromMap(data, documentId),
      sort: 'title',
      descending: false,
    );
  }

  // make a new habit
  Future<void> setHabitListItem(HabitModel item) async {
    await _setData(
      path: APIPath.habit(uid, item.id),
      data: item.toMap(),
    );
  }

  // delete an item
  Future<void> deleteHabitListItem(HabitModel item) async => await _deleteData(
        path: APIPath.habit(uid, item.id),
      );

  // ## Helpers
  Future<void> _setData({String path, Map<String, dynamic> data}) async {
    final reference = Firestore.instance.document(path);
    print('$path: $data');

    await reference.setData(data);
  }

  Future<void> _deleteData({String path}) async {
    final reference = Firestore.instance.document(path);
    await reference.delete();
  }

  Stream<List<T>> _collectionStream<T>({
    @required String path,
    @required String sort,
    @required bool descending,
    @required T builder(Map<String, dynamic> data, String documentId),
  }) {
    final reference = Firestore.instance
        .collection(path)
        .orderBy(sort, descending: descending);
    final snapshots = reference.snapshots();

    return snapshots.map((snapshot) =>
        snapshot.documents.map((s) => builder(s.data, s.documentID)).toList());
  }
}
