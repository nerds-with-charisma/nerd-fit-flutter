import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';

// the user class
class User {
  User({@required this.uid, this.username, this.email, this.avatar});
  final String uid;
  final String username;
  final String email;
  final String avatar;
}

// abstract our functions from below
abstract class AuthBase {
  Stream<User> get onAuthStateChanged;
  Future<User> currentUser();
  Future<User> signInAnon();
  Future<void> signOut();
  Future<User> signInWithGoogle();
  Future<User> signInWithEmailAndPassword(String email, String password);
  Future<User> createUserWithEmailAndPassword(String email, String password);
}

// the auth class
class Auth implements AuthBase {
  // init our firebase instance
  final _fbAuth = FirebaseAuth.instance;

  // stream to get authentication changes
  @override
  Stream<User> get onAuthStateChanged {
    return _fbAuth.onAuthStateChanged.map((user) => _userFromFirebase(user));
  }

  // email sign in
  @override
  Future<User> signInWithEmailAndPassword(String email, String password) async {
    final authResult = await _fbAuth.signInWithEmailAndPassword(
        email: email, password: password);

    return _userFromFirebase(authResult.user);
  }

  @override
  Future<User> createUserWithEmailAndPassword(
      String email, String password) async {
    final authResult = await _fbAuth.createUserWithEmailAndPassword(
        email: email, password: password);

    return _userFromFirebase(authResult.user);
  }

  // get fb user info
  User _userFromFirebase(FirebaseUser user) {
    if (user == null) return null;

    return User(
      uid: user.uid,
      username: user.displayName,
      email: user.email,
      avatar: user.photoUrl,
    );
  }

  // get current user info
  @override
  Future<User> currentUser() async {
    final user = await _fbAuth.currentUser();
    return _userFromFirebase(user);
  }

  // sign in anonymously
  @override
  Future<User> signInAnon() async {
    final authResult = await _fbAuth.signInAnonymously();
    return _userFromFirebase(authResult.user);
  }

  // google sign in
  Future<User> signInWithGoogle() async {
    GoogleSignIn googleSignIn = GoogleSignIn();
    GoogleSignInAccount googleAccount = await googleSignIn.signIn();
    if (googleAccount != null) {
      GoogleSignInAuthentication googleAuth =
          await googleAccount.authentication;

      if (googleAuth.accessToken != null && googleAuth.idToken != null) {
        final authResult =
            await _fbAuth.signInWithCredential(GoogleAuthProvider.getCredential(
          idToken: googleAuth.idToken,
          accessToken: googleAuth.accessToken,
        ));

        return _userFromFirebase(authResult.user);
      } else {
        throw PlatformException(
          code: 'ERROR_MISSING_TOKEN',
          message: 'Missing Google auth token',
        );
      }
    } else {
      throw PlatformException(
        code: 'ERROR_ABORTED',
        message: 'Sign in aborted',
      );
    }
  }

  // sign out all types
  @override
  Future<void> signOut() async {
    // google signout
    final googleSignIn = GoogleSignIn();
    await googleSignIn.signOut();

    // anon signout
    await _fbAuth.signOut();
  }
}
