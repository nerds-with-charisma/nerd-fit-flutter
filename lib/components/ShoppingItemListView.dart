import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:nerd_fit/components/EmptyShoppingLIst.dart';
import 'package:nerd_fit/components/ShoppingListItemDisplay.dart';
import 'package:nerd_fit/models/ShoppingListItem.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class ShoppingItemListView extends StatelessWidget {
  ShoppingItemListView({
    this.filter,
    this.sort,
    this.descending,
  });

  final bool filter;
  final String sort;
  final bool descending;

  @override
  Widget build(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);
    return StreamBuilder<List<ShoppingListItem>>(
      stream: database.shoppingListItemStream(
        sort: sort,
        descending: descending,
      ),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final items = snapshot.data;

          if (items.isNotEmpty) {
            final children = items
                .map((item) => ShoppingListItemDisplay(
                      filter: filter,
                      item: item,
                      index: items.indexOf(item),
                      onTap: () async {
                        log(item.title);
                        await database.setShoppingListItem(ShoppingListItem(
                          id: item.id,
                          title: item.title,
                          isNeeded: !item.isNeeded,
                          date: (item.date != null)
                              ? item.date
                              : DateTime.now().toIso8601String(),
                        ));
                      },
                    ))
                .toList();

            return Expanded(
              child: ListView(
                children: children,
              ),
            );
          } else {
            return EmptyShoppingList();
          }
        }
        if (snapshot.hasError) {
          return Center(child: Text('Some error occurred'));
        }

        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
