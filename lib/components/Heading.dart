import 'package:flutter/material.dart';

class Heading extends StatelessWidget {
  final String title;

  Heading({this.title});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 42.0,
        fontWeight: FontWeight.w100,
        color: Colors.white,
      ),
    );
  }
}
