import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class WorkoutActions extends StatelessWidget {
  WorkoutActions();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      color: Colors.black,
      icon: Icon(Icons.queue_music),
      onPressed: () async {
        if (await canLaunch('https://sptfy.com/71Gb')) {
          await launch('https://sptfy.com/71Gb');
        } else {
          throw "Could not launch spotify url";
        }
      },
    );
  }
}
