handleShoppingListDropdown(int v, bool filter, String sort, bool descending) {
  dynamic data = {};

  if (v == 0) {
    data['filter'] = !filter;
    data['sort'] = sort;
    data['descending'] = descending;
  }

  if (v == 1) {
    data['filter'] = filter;
    data['sort'] = 'title';
    data['descending'] = false;
  }

  if (v == 2) {
    data['filter'] = filter;
    data['sort'] = 'date';
    data['descending'] = true;
  }

  if (v == 3) {
    data['filter'] = filter;
    data['sort'] = 'isNeeded';
    data['descending'] = true;
  }

  return data;
}
