import 'package:flutter/material.dart';

class CustomElevatedButton extends StatelessWidget {
  @required
  final VoidCallback onPressed;
  @required
  final String title;

  final Color bgColor;
  final double height;
  final String icon;
  final double radius;
  final Color textColor;

  CustomElevatedButton({
    this.bgColor,
    this.height: 50.0,
    this.icon,
    this.onPressed,
    this.radius: 0.0,
    this.textColor,
    this.title,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shadowColor: Colors.black,
          elevation: 8,
          primary: bgColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(radius),
            ),
          ),
        ),
        child: Row(
          children: <Widget>[
            if (icon != null)
              Container(
                child: Image.asset(
                  icon,
                  height: 30,
                ),
                padding: EdgeInsetsDirectional.only(
                  end: 10,
                ),
              ),
            Text(
              title,
              style: TextStyle(
                  color: textColor,
                  fontSize: 16.0,
                  fontWeight: FontWeight.normal),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
        onPressed: onPressed,
      ),
    );
  }
}
