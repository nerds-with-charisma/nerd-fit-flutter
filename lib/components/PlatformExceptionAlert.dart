import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nerd_fit/components/platform_alert_dialoge.dart';

class PlatformExceptionAlert extends PlatformAlertDialog {
  PlatformExceptionAlert({
    @required String title,
    @required PlatformException exception,
  }) : super(
          title: title,
          content: _message(exception),
          defaultActionText: 'OK',
        );

  static String _message(PlatformException exception) {
    print(exception);
    if (exception.message ==
        'PERMISSION_DENIED: Missing or insufficient permissions.') {
      return 'Oops, missing or  insufficient permissions';
    }
    return _errors[exception.code] ?? exception.message;
  }

  static Map<String, String> _errors = {
    'ERROR_WEAK_PASSWORD': 'Your password is not strong enough.',
    'ERROR_INVALID_EMAIL': 'Email address is malformed.',
    'ERROR_EMAIL_ALREADY_IN_USE':
        'This email is already in use by a different account.',
    'ERROR_INVALID_CREDENTIAL': 'Invalid login.',
    'ERROR_WRONG_PASSWORD': 'Incorrect password.',
    'ERROR_USER_NOT_FOUND': 'Invalid login.',
    'ERROR_USER_DISABLED': 'Your account is suspended.',
    'ERROR_TOO_MANY_REQUESTS': 'Please wait to attempt another signin.',
    'ERROR_OPERATION_NOT_ALLOWED': 'Email & Password accounts are not enabled.',
  };
}
