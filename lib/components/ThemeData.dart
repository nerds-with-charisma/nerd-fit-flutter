// :: todo

import 'package:flutter/material.dart';

ThemeData myThemeData() {
  var radii = BorderRadius.all(Radius.circular(10));

  return ThemeData(
    primarySwatch: Colors.indigo,
    primaryColor: Colors.redAccent[600], // red
    primaryColorLight: Colors.pink[600],
    // accentColor: Colors.purple[600],
    splashColor: Colors.black,
    errorColor: Colors.red[400],
    hintColor: Colors.tealAccent,

    textTheme: ThemeData.light().textTheme.copyWith(
          bodyText1: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
          bodyText2: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
        ),

    // button styles
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        onPrimary: Colors.white,
        primary: Colors.cyan[300],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        minimumSize: Size(double.infinity, 40),
      ),
    ),

    textButtonTheme: TextButtonThemeData(
      style: ElevatedButton.styleFrom(
        onPrimary: Colors.white,
        minimumSize: Size(double.infinity, 40),
      ),
    ),

    // text field styles
    inputDecorationTheme: InputDecorationTheme(
      contentPadding: EdgeInsets.all(10),
      filled: true,
      fillColor: Colors.white,
      labelStyle: TextStyle(
        color: Colors.blueGrey,
      ),
      border: new OutlineInputBorder(
        borderRadius: radii,
      ),
      // normal status
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
        borderRadius: radii,
      ),
      // error styles
      errorStyle: TextStyle(
        color: Colors.pinkAccent,
      ),
      errorBorder: new OutlineInputBorder(
        borderSide: new BorderSide(color: Colors.pinkAccent),
        borderRadius: radii,
      ),
      focusedErrorBorder: new OutlineInputBorder(
        // borderSide: BorderSide.none,
        borderRadius: radii,
      ),
      // when focues/active
      focusedBorder: OutlineInputBorder(
        borderSide: new BorderSide(color: Colors.tealAccent[100]),
        borderRadius: radii,
      ),
      hintStyle: TextStyle(
        color: Colors.tealAccent[100],
        fontSize: 14.0,
      ),
    ),
  );
}
