import 'package:flutter/material.dart';

class ShoppingListSortDropdown extends StatelessWidget {
  ShoppingListSortDropdown({this.filter, this.sort, this.callback});
  final bool filter;
  final String sort;
  final Function callback;

  final styles = TextStyle(color: Colors.black, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PopupMenuButton(
        icon:
            Icon(Icons.filter_list), //don't specify icon if you want 3 dot menu
        color: Colors.white,
        padding: EdgeInsets.all(0),
        itemBuilder: (context) => [
          PopupMenuItem<int>(
            height: 20,
            value: 0,
            child: TextButton.icon(
              onPressed: null,
              icon: Icon(Icons.shopping_bag_outlined,
                  color: filter == true ? Colors.purpleAccent[700] : null),
              label: filter == true
                  ? Text('Show all items', style: styles)
                  : Text(
                      'Show only needed',
                      style: styles,
                    ),
            ),
          ),
          PopupMenuItem<int>(
            height: 20,
            value: 1,
            child: TextButton.icon(
              onPressed: null,
              icon: Icon(Icons.sort_by_alpha,
                  color: sort == 'title' ? Colors.purpleAccent[700] : null),
              label: Text(
                'Sort Alphabetically',
                style: styles,
              ),
            ),
          ),
          PopupMenuItem<int>(
            height: 20,
            value: 2,
            child: TextButton.icon(
              onPressed: null,
              icon: Icon(Icons.lock_clock_outlined,
                  color: sort == 'date' ? Colors.purpleAccent[700] : null),
              label: Text(
                'Sort By Date',
                style: styles,
              ),
            ),
          ),
          PopupMenuItem<int>(
            height: 20,
            value: 3,
            child: TextButton.icon(
              onPressed: null,
              icon: Icon(Icons.shopping_basket_outlined,
                  color: sort == 'isNeeded' ? Colors.purpleAccent[700] : null),
              label: Text(
                'Sort By Needed',
                style: styles,
              ),
            ),
          ),
        ],
        onSelected: (item) => {callback(item)},
      ),
    );
  }
}
