import 'package:flutter/material.dart';
import 'package:nerd_fit/models/ShoppingListItem.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class ShoppingListItemDisplay extends StatelessWidget {
  ShoppingListItemDisplay({
    @required this.item,
    @required this.index,
    @required this.onTap,
    this.filter,
  });
  final ShoppingListItem item;
  final int index;
  final VoidCallback onTap;
  final bool filter;

  @override
  Widget build(BuildContext context) {
    if (filter == true && item.isNeeded != true) return SizedBox.shrink();

    return Dismissible(
      background: Container(
        alignment: Alignment.centerRight,
        color: Colors.pinkAccent[400],
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
        padding: EdgeInsets.only(right: 20),
      ),
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Are you sure?'),
            content: Text(
                'Do you want to delete this item? You can always mark it as not needed instead.'),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.of(ctx).pop(false);
                  },
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  )),
              TextButton(
                  onPressed: () {
                    Navigator.of(ctx).pop(true);
                  },
                  child: Text(
                    'Confirm Delete',
                    style: TextStyle(
                      color: Colors.redAccent,
                    ),
                  ))
            ],
          ),
        );
      },
      direction: DismissDirection.endToStart,
      onDismissed: (direction) async {
        try {
          final database = Provider.of<Database>(context, listen: false);
          await database.deleteShoppingListItem(item);
        } catch (e) {
          print(e);
        }
      },
      key: UniqueKey(),
      child: Container(
        color: index.isEven ? Colors.white : Colors.grey[100],
        child: ListTile(
          dense: true,
          contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
          subtitle: item.isNeeded == true
              ? Text(
                  'I Need It',
                  style: TextStyle(
                    fontSize: 10.0,
                  ),
                )
              : Text(
                  'Don\'t Need It ATM',
                  style: TextStyle(
                    color: Colors.blueGrey[100],
                    fontSize: 10.0,
                  ),
                ),
          title: Text(
            item.title,
            style: TextStyle(
                decoration:
                    item.isNeeded == true ? null : TextDecoration.lineThrough,
                fontSize: 16.0,
                color: item.isNeeded == true
                    ? Colors.black
                    : Colors.blueGrey[600]),
          ),
          trailing: Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 9,
                  spreadRadius: 0.1,
                  offset: const Offset(0, 2),
                ),
              ],
              borderRadius: BorderRadius.circular(10),
            ),
            child: CircleAvatar(
              backgroundColor:
                  item.isNeeded == true ? Color(0xffB1FF1D) : Colors.white,
              child: IconButton(
                  color: Colors.black,
                  icon: Icon(
                      item.isNeeded == true ? Icons.check_sharp : Icons.add),
                  onPressed: () {
                    onTap();

                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Shopping Item Toggled'),
                        duration: Duration(seconds: 2),
                      ),
                    );
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
