import 'package:flutter/material.dart';

class NavItem extends StatelessWidget {
  NavItem({
    @required this.icon,
    @required this.isActive,
    @required this.title,
    @required this.callback,
  });

  final Object icon;
  final bool isActive;
  final String title;
  final Function callback;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      tileColor: isActive ? Colors.grey[50] : Colors.white,
      leading: Icon(
        icon,
        color: isActive ? Colors.purple : Colors.blueGrey[100],
      ),
      title: Text(title,
          style: TextStyle(
            fontWeight: isActive ? FontWeight.bold : FontWeight.normal,
          )),
      onTap: () => callback(),
    );
  }
}
