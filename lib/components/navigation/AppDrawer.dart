import 'package:flutter/material.dart';

import 'package:nerd_fit/app/home/TabItem.dart';

import 'package:nerd_fit/components/auth/getuserData.dart';
import 'package:nerd_fit/components/navigation/VersionInfo.dart';
import 'package:nerd_fit/components/MarginBottom.dart';
import 'package:nerd_fit/components/navigation/NavItem.dart';

import 'package:nerd_fit/utils/checkForUpdates.dart';
import 'package:nerd_fit/utils/confirmSignoutModal.dart';
import 'package:nerd_fit/utils/setStartingScreen.dart';

class AppDrawer extends StatefulWidget {
  AppDrawer({
    @required this.currentTab,
    @required this.onSelectTab,
  });

  final TabItem currentTab;
  final Function onSelectTab;

  @override
  State<AppDrawer> createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  @override
  Widget build(BuildContext context) {
    bool _hasLoaded = false;
    dynamic userProfileData = null;

    Future<void> _init() async {
      if (_hasLoaded != true) {
        dynamic userData = await getUserData(context);
        if (userData != null) {
          setState(() {
            userProfileData = userData[0];
            _hasLoaded = true;
          });
        }
      }
    }

    _init();

    return Drawer(
      backgroundColor: Colors.white,
      child: Column(
        children: <Widget>[
          MarginBottom(),
          userProfileData != null
              ? NavItem(
                  icon: Icons.fitness_center_outlined,
                  isActive: widget.currentTab == TabItem.workouts,
                  title: 'Your Current Workout',
                  callback: () =>
                      setStartingScreen(TabItem.workouts, widget.onSelectTab),
                )
              : NavItem(
                  icon: Icons.account_circle_outlined,
                  isActive: widget.currentTab == TabItem.profile,
                  title: 'Setup Your Fitness Profile',
                  callback: () =>
                      setStartingScreen(TabItem.profile, widget.onSelectTab),
                ),
          userProfileData != null
              ? NavItem(
                  icon: Icons.calendar_today,
                  isActive: widget.currentTab == TabItem.progress,
                  title: 'Your Workout Schedule',
                  callback: () =>
                      setStartingScreen(TabItem.progress, widget.onSelectTab),
                )
              : SizedBox(),
          NavItem(
            icon: Icons.shopping_basket_outlined,
            isActive: widget.currentTab == TabItem.shoppingList,
            title: 'Shopping List',
            callback: () =>
                setStartingScreen(TabItem.shoppingList, widget.onSelectTab),
          ),
          NavItem(
            icon: Icons.check_box_outlined,
            isActive: widget.currentTab == TabItem.habits,
            title: 'Habit Tracker',
            callback: () =>
                setStartingScreen(TabItem.habits, widget.onSelectTab),
          ),
          Divider(),
          ExpansionTile(
            leading: Icon(
              Icons.settings_outlined,
              color: Colors.blueGrey[100],
            ),
            title: Text('Profile & Settings'),
            textColor: Colors.black,
            iconColor: Colors.black,
            controlAffinity: ListTileControlAffinity.trailing,
            children: <Widget>[
              userProfileData != null
                  ? SizedBox
                  : NavItem(
                      icon: Icons.account_circle_outlined,
                      isActive: widget.currentTab == TabItem.profile,
                      title: 'Fitness Profile',
                      callback: () => setStartingScreen(
                          TabItem.profile, widget.onSelectTab),
                    ),
              NavItem(
                icon: Icons.timeline_outlined,
                isActive: widget.currentTab == TabItem.dimensions,
                title: 'Log Body Dimensions',
                callback: () =>
                    setStartingScreen(TabItem.dimensions, widget.onSelectTab),
              ),
              NavItem(
                icon: Icons.lightbulb_outline_rounded,
                isActive: widget.currentTab == TabItem.tips,
                title: 'Fitness Tips',
                callback: () =>
                    setStartingScreen(TabItem.tips, widget.onSelectTab),
              ),
              NavItem(
                  icon: Icons.system_update,
                  isActive: false,
                  title: 'Check for Update',
                  callback: () async {
                    checkForUpdates(context);
                  }),
              NavItem(
                icon: Icons.logout_outlined,
                isActive: false,
                title: 'Logout',
                callback: () => confirmSignOut(context),
              ),
            ],
          ),
          VersionInfo(),
          MarginBottom(),
        ],
      ),
    );
  }
}
