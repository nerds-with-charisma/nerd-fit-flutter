import 'package:flutter/material.dart';

class LodingIndicator extends StatelessWidget {
  LodingIndicator(this.isLoading);
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.minPositive,
      color: Colors.black.withOpacity(0.7),
      child: isLoading
          ? Center(
              child: CircularProgressIndicator(
                strokeWidth: 1,
                color: Colors.white,
              ),
            )
          : SizedBox(),
    );
  }
}
