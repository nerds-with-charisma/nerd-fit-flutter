import 'package:flutter/material.dart';
import 'package:nerd_fit/components/MarginBottom.dart';

class CustomTextField extends StatelessWidget {
  CustomTextField({
    this.autocorrect,
    this.controller,
    this.isValid,
    this.labelText,
    this.errorText,
    this.focusNode,
    this.obscureText,
    this.callback,
    this.onEditingComplete,
    this.action,
    this.hintText,
    this.keyboardType,
  });

  final autocorrect;
  final controller;
  final isValid;
  final labelText;
  final errorText;
  final focusNode;
  final obscureText;
  final callback;
  final onEditingComplete;
  final action;
  final hintText;
  final keyboardType;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: Offset(0.0, 0.1), //(x,y)
                blurRadius: 8.0,
              ),
            ],
          ),
          child: TextField(
            autocorrect: autocorrect,
            controller: controller,
            decoration: InputDecoration(
              errorText: isValid ? errorText : null,
              labelText: labelText,
            ),
            focusNode: focusNode,
            obscureText: obscureText,
            onChanged: (val) => callback(val),
            onEditingComplete: onEditingComplete,
            textInputAction: action,
          ),
        ),
        MarginBottom(),
      ],
    );
  }
}
