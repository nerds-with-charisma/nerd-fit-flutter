import 'package:flutter/material.dart';

class NwcLogo extends StatelessWidget {
  NwcLogo({this.height});
  final height;

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      'assets/images/logo--nwc.png',
      height: height.toDouble(),
    );
  }
}
