import 'package:flutter/material.dart';

class GradientButton extends StatelessWidget {
  GradientButton({this.title, this.callback});
  final String title;
  final Function callback;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Color(0xffBA00FF).withOpacity(0.25),
            offset: Offset(0.0, 5.0), //(x,y)
            blurRadius: 4.0,
          ),
        ],
        gradient: new LinearGradient(
          colors: [Color(0xffFF0553), Color(0xffBA00FF)],
          begin: FractionalOffset.centerLeft,
          end: FractionalOffset.centerRight,
        ),
      ),
      child: TextButton(
        child: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
        onPressed: callback,
      ),
    );
  }
}
