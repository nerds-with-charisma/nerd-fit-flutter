import 'package:flutter/material.dart';

class EmptyShoppingList extends StatelessWidget {
  EmptyShoppingList({this.title = 'Nothing to see here'});
  final String title;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Center(
        child: Text(title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 28.0,
            )),
      ),
    );
  }
}
