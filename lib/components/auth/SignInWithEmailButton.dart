import 'package:flutter/material.dart';
import 'package:nerd_fit/components/auth/SignInBloc.dart';
import 'package:nerd_fit/components/auth/SignInButton.dart';
import 'package:nerd_fit/screens/EmailLoginScreen.dart';

class SignInWithEmailButton extends StatelessWidget {
  final SignInBloc bloc;
  final bool isLoading;

  SignInWithEmailButton({this.bloc, this.isLoading});

  @override
  Widget build(BuildContext context) {
    void _signInWithEmail(BuildContext context) {
      Navigator.of(context).push(
        MaterialPageRoute<void>(
          fullscreenDialog: true,
          builder: (context) => EmailLoginScreen(),
        ),
      );
    }

    return SignInButton(
      bgColor: Colors.white,
      icon: 'assets/images/email-logo.png',
      onPressed: isLoading ? null : () => _signInWithEmail(context),
      radius: 10,
      textColor: Colors.black,
      title: 'Continue with Email',
    );
  }
}
