import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nerd_fit/components/auth/SignInBloc.dart';
import 'package:nerd_fit/components/auth/SignInButton.dart';
import 'package:nerd_fit/components/PlatformExceptionAlert.dart';

class SignInWithGoogleButton extends StatelessWidget {
  final SignInBloc bloc;
  final bool isLoading;

  SignInWithGoogleButton({this.bloc, this.isLoading});

  @override
  Widget build(BuildContext context) {
    void _showSignInError(BuildContext context, PlatformException exception) {
      PlatformExceptionAlert(
        title: 'Sign in failed',
        exception: exception,
      ).show(context);
    }

    Future<void> _signInWithGoogle(BuildContext context) async {
      try {
        await bloc.signInWithGoogle();
      } on PlatformException catch (e) {
        _showSignInError(context, e);
      }
    }

    return SignInButton(
      bgColor: Colors.white,
      icon: 'assets/images/google-logo.png',
      onPressed: isLoading ? null : () => _signInWithGoogle(context),
      textColor: Colors.black,
      radius: 10,
      title: 'Sign-In with Google',
    );
  }
}
