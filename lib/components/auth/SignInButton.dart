import 'package:flutter/material.dart';
import 'package:nerd_fit/components/CustomElevatedButton.dart';

class SignInButton extends CustomElevatedButton {
  SignInButton({
    Color bgColor,
    String icon,
    VoidCallback onPressed,
    Color textColor,
    String title,
    double radius,
  })  : assert(title != null),
        super(
            bgColor: bgColor,
            height: 50.0,
            icon: icon,
            onPressed: onPressed,
            textColor: textColor,
            title: title,
            radius: radius);
}
