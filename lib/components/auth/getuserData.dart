import 'package:flutter/material.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

Future<dynamic> getUserData(BuildContext context) async {
  final database = await Provider.of<Database>(context, listen: false);
  var userData = await database.getUserDataStream().first;

  if (userData.length > 0) {
    return userData[0];
  }
}
