import 'package:flutter/material.dart';

class SignInLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      'assets/images/logo--signIn.png',
      height: 100.0,
    );
  }
}
