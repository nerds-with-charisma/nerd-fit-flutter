import 'package:flutter/material.dart';

class MarginBottom extends StatelessWidget {
  MarginBottom({this.gap = 20});
  final double gap;

  @override
  Widget build(BuildContext context) {
    return SizedBox(height: gap);
  }
}
