import 'package:flutter/material.dart';

class Input extends StatelessWidget {
  final Function callback;
  final controller;
  final String error;
  final String label;
  final bool obscureText;

  Input({
    this.callback,
    this.controller,
    this.error,
    this.label,
    this.obscureText = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10,
            offset: const Offset(0, -3),
          ),
        ],
      ),
      child: TextField(
        controller: controller,
        onChanged: (v) {
          callback(v);
        },
        obscureText: obscureText,
        decoration: InputDecoration(
          labelText: label,
          labelStyle: TextStyle(
            color: Colors.black87,
          ),
          errorText: error,
        ),
        style: TextStyle(
          fontSize: 14.0,
        ),
      ),
    );
  }
}
