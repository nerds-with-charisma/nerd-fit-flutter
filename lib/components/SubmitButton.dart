import 'package:flutter/material.dart';
import 'package:nerd_fit/components/CustomElevatedButton.dart';

class SubmitButton extends CustomElevatedButton {
  SubmitButton({
    @required String title,
    VoidCallback onPressed,
    double radius = 0.0,
    bg = Colors.black,
    text = Colors.white,
  }) : super(
          bgColor: bg,
          title: title,
          height: 44.0,
          textColor: text,
          radius: radius,
          onPressed: onPressed,
        );
}
