import 'package:flutter/material.dart';
import 'package:nerd_fit/models/ShoppingListItem.dart';
import 'package:nerd_fit/utils/shoppingListCrud/createShoppingListItem.dart';

class AddShoppingListItem extends StatefulWidget {
  @override
  State<AddShoppingListItem> createState() => _AddShoppingListItemState();
}

class _AddShoppingListItemState extends State<AddShoppingListItem> {
  _AddShoppingListItemState({this.item});
  final _addItemInputController = TextEditingController();
  final _form = GlobalKey<FormState>();
  final ShoppingListItem item;

  String _title = null;

  bool _validateAndSaveForm() {
    final form = _form.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }

    return false;
  }

  Future<void> _createShoppingListItemFunc(BuildContext context) async {
    if (_validateAndSaveForm()) {
      final res = await createShoppingListItem(item, _title, context);
      if (res == true) {
        _addItemInputController.clear(); // clear the form
        FocusScope.of(context).requestFocus(FocusNode()); // remove focus
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.07),
            blurRadius: 10,
            offset: const Offset(0, 10),
          ),
        ],
      ),
      padding: EdgeInsets.all(10),
      child: Form(
        child: TextFormField(
          decoration: InputDecoration(
            border: InputBorder.none,
            labelText: 'Add Something',
            suffixIcon: IconButton(
              color: Colors.black,
              icon: Icon(Icons.arrow_forward),
              onPressed: () => _createShoppingListItemFunc(context),
            ),
          ),
          textInputAction: TextInputAction.done,
          textCapitalization: TextCapitalization.sentences,
          controller: _addItemInputController,
          onFieldSubmitted: (_) {
            _createShoppingListItemFunc(context);
          },
          onSaved: (value) => _title = value,
          validator: (value) {
            if (value.isEmpty) return 'Please add something';
            return null;
          },
        ),
        key: _form,
      ),
    );
  }
}
