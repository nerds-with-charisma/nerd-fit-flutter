import 'package:flutter/material.dart';
import 'package:nerd_fit/utils/statics.dart';

AppBar mainAppBar({String title, List<Widget> actions}) {
  return AppBar(
    centerTitle: true,
    actions: actions,
    backgroundColor: Colors.white,
    elevation: 3,
    iconTheme: IconThemeData(color: Colors.black),
    title: Container(
      child: title == null
          ? Image.asset(
              statics()['logoGradient'],
              height: 30,
            )
          : Text(title,
              style: TextStyle(
                color: Colors.black,
                fontSize: 14.0,
              )),
    ),
  );
}
