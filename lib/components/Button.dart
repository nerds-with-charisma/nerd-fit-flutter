// import 'package:flutter/material.dart';

// class Button extends StatelessWidget {
//   final String title;
//   final Function callback;

//   Button({this.title, this.callback});

//   @override
//   Widget build(BuildContext context) {
//     return ElevatedButton(
//       onPressed: callback,
//       child: Text(title),
//       style: ElevatedButton.styleFrom(
//         elevation: 5,
//         onPrimary: Colors.white,
//         animationDuration: Duration(milliseconds: 1000),
//       ),
//     );
//   }
// }
