import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nerd_fit/components/auth/SignInBloc.dart';
import 'package:nerd_fit/components/auth/SignInButton.dart';

class SignInAnonButton extends StatelessWidget {
  final SignInBloc bloc;
  final bool isLoading;

  SignInAnonButton({this.bloc, this.isLoading});

  @override
  Widget build(BuildContext context) {
    Future<void> _signInAnon(BuildContext context) async {
      try {
        await bloc.signInAnon();
      } on PlatformException catch (e) {
        print(e);
      }
    }

    return SignInButton(
      bgColor: Colors.pinkAccent[400],
      onPressed: isLoading ? null : () => _signInAnon(context),
      radius: 300,
      textColor: Colors.white,
      title: 'Sign In Anonymously',
    );
  }
}
