import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nerd_fit/components/MarginBottom.dart';
import 'package:nerd_fit/components/PlatformExceptionAlert.dart';
import 'package:nerd_fit/components/SubmitButton.dart';
import 'package:nerd_fit/models/EmailSignInModel.dart';
import 'package:nerd_fit/providers/auth.dart';
import 'package:nerd_fit/utils/validators.dart';
import 'package:provider/provider.dart';

class SigninForm extends StatefulWidget with EmailAndPasswordValidators {
  @override
  State<SigninForm> createState() => _SigninFormState();
}

class _SigninFormState extends State<SigninForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  String get _email => _emailController.text;
  String get _password => _passwordController.text;

  bool _submitted = false;
  bool _loading = false;

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    print('disposed of');
    super.dispose();
  }

  EmailSignInFormType _formType = EmailSignInFormType.signIn;

  void _emailEditingComplete() {
    // if there's an error, stay on the email input
    final newFocus = widget.emailValidator.isValid(_email)
        ? _passwordFocusNode
        : _emailFocusNode;

    FocusScope.of(context).requestFocus(newFocus);
  }

  void _toggleFormType() {
    setState(() {
      _submitted = false;
      _formType = _formType == EmailSignInFormType.signIn
          ? EmailSignInFormType.register
          : EmailSignInFormType.signIn;
    });

    _emailController.clear();
    _passwordController.clear();
  }

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthBase>(context);

    void _submit() async {
      setState(() {
        setState(() => _loading = true);
        _submitted = true;
      });
      try {
        if (_formType == EmailSignInFormType.signIn) {
          print('$_email :: $_password');
          await auth.signInWithEmailAndPassword(_email, _password);
        } else {
          await auth.createUserWithEmailAndPassword(_email, _password);
        }

        Navigator.of(context).pop();
      } on PlatformException catch (e) {
        PlatformExceptionAlert(
          title: 'Sign in  failed',
          exception: e,
        ).show(context);
      } finally {
        setState(() => _loading = false);
      }
    }

    _updateState() {
      setState(() {});
    }

    List<Widget> _buildChildren() {
      final primaryText = _formType == EmailSignInFormType.signIn
          ? 'Sign In'
          : 'Create an account';

      final secondaryText = _formType == EmailSignInFormType.signIn
          ? 'Need an account? Register now.'
          : 'Have an account? Sign in.';

      bool submitEnabled = widget.emailValidator.isValid(_email) &&
          widget.emailValidator.isValid(_password);

      bool emailValid = _submitted && !widget.emailValidator.isValid(_email);
      bool passwordValid =
          _submitted && !widget.emailValidator.isValid(_password);

      return [
        MarginBottom(),
        Image.asset(
          'assets/images/logo--purple-to-pink.png',
          height: 30.0,
        ),
        MarginBottom(),
        TextField(
          controller: _emailController,
          decoration: InputDecoration(
            errorText: emailValid ? widget.invalidEmailErrorText : null,
            hintText: 'test@nwc.com',
            labelText: 'Your Email',
          ),
          focusNode: _emailFocusNode,
          keyboardType: TextInputType.emailAddress,
          onChanged: (email) => _updateState(),
          onEditingComplete: _emailEditingComplete,
          textInputAction: TextInputAction.next,
        ),
        MarginBottom(),
        TextField(
          autocorrect: false,
          controller: _passwordController,
          decoration: InputDecoration(
            errorText: passwordValid ? widget.invalidPasswordErrorText : null,
            labelText: 'Your Password',
          ),
          focusNode: _passwordFocusNode,
          obscureText: true,
          onChanged: (password) => _updateState(),
          onEditingComplete: _submit,
          textInputAction: TextInputAction.done,
        ),
        MarginBottom(),
        SubmitButton(
          title: primaryText,
          onPressed: submitEnabled ? _submit : null,
        ),
        TextButton(
          child: Text(
            secondaryText,
            style: TextStyle(
              color: Colors.black,
              fontSize: 12.0,
              fontWeight: FontWeight.normal,
            ),
          ),
          onPressed: _toggleFormType,
        ),
      ];
    }

    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        children: (_loading == true)
            ? [
                Container(
                  child: CircularProgressIndicator(),
                )
              ]
            : _buildChildren(),
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
      ),
    );
  }
}
