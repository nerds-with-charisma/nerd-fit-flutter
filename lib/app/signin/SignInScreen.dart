import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:nerd_fit/utils/getAuth.dart';
import 'package:nerd_fit/utils/statics.dart';

import 'package:nerd_fit/components/auth/SignInBloc.dart';
import 'package:nerd_fit/components/auth/SignInLogo.dart';
import 'package:nerd_fit/components/auth/SignInWithEmailButton.dart';
import 'package:nerd_fit/components/auth/SignInWithGoogleButton.dart';
import 'package:nerd_fit/components/MarginBottom.dart';
import 'package:nerd_fit/components/common/NwcLogo.dart';

class SignInScreen extends StatelessWidget {
  SignInScreen({@required this.bloc, @required this.isLoading});

  final SignInBloc bloc;
  final bool isLoading;

  static Widget create(BuildContext context) {
    final auth = getAuth(context);

    return ChangeNotifierProvider<ValueNotifier<bool>>(
      create: (_) => ValueNotifier<bool>(false),
      child: Consumer<ValueNotifier<bool>>(
        builder: (_, isLoading, __) => Provider<SignInBloc>(
          create: (_) => SignInBloc(
            auth: auth,
            isLoading: isLoading,
          ),
          child: Consumer<SignInBloc>(
            builder: (context, bloc, _) => SignInScreen(
              bloc: bloc,
              isLoading: isLoading.value,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildContent(context),
      backgroundColor: Colors.grey[200],
    );
  }

  Widget _buildContent(BuildContext context) {
    return Container(
      child: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(statics()['standardBackground']),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            // ## nwc logo
            NwcLogo(height: 25),
            MarginBottom(gap: 200),

            // ## Signin Logo
            SignInLogo(),
            MarginBottom(gap: 170),

            // ## Sign with google in button
            SignInWithGoogleButton(
              bloc: bloc,
              isLoading: isLoading,
            ),
            MarginBottom(),

            // ## Sign in with email button
            SignInWithEmailButton(
              bloc: bloc,
              isLoading: isLoading,
            ),
          ],
        ),
      ),
    );
  }
}
