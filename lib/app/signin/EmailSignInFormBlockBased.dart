import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nerd_fit/components/common/CustomTextField.dart';
import 'package:nerd_fit/components/common/GradientButton.dart';
import 'package:provider/provider.dart';

import 'package:nerd_fit/models/EmailSignInModel.dart';
import 'package:nerd_fit/providers/auth.dart';

import 'package:nerd_fit/app/signin/EmailSignInBloc.dart';
import 'package:nerd_fit/components/PlatformExceptionAlert.dart';
import 'package:nerd_fit/components/auth/SignInLogo.dart';

class SigninFormBlocBased extends StatefulWidget {
  SigninFormBlocBased({
    @required this.bloc,
  });

  final EmailSignInBloc bloc;

  static Widget create(BuildContext context) {
    final AuthBase auth = Provider.of<AuthBase>(context);
    return Provider<EmailSignInBloc>(
      create: (context) => EmailSignInBloc(auth: auth),
      child: Consumer<EmailSignInBloc>(
        builder: (context, bloc, _) => SigninFormBlocBased(bloc: bloc),
      ),
      dispose: (context, bloc) => bloc.dispose(),
    );
  }

  @override
  State<SigninFormBlocBased> createState() => _SigninFormBlocBasedState();
}

class _SigninFormBlocBasedState extends State<SigninFormBlocBased> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    super.dispose();
  }

  void _emailEditingComplete(EmailSignInModel model) {
    // if there's an error, stay on the email input
    final newFocus = model.emailValidator.isValid(model.email)
        ? _passwordFocusNode
        : _emailFocusNode;

    FocusScope.of(context).requestFocus(newFocus);
  }

  void _toggleFormType(EmailSignInModel model) {
    widget.bloc.toggleFormType();

    _emailController.clear();
    _passwordController.clear();
  }

  @override
  Widget build(BuildContext context) {
    void _submit() async {
      try {
        await widget.bloc.submit();

        Navigator.of(context).pop();
      } on PlatformException catch (e) {
        PlatformExceptionAlert(
          title: 'Sign in failed',
          exception: e,
        ).show(context);
      }
    }

    List<Widget> _buildChildren(EmailSignInModel model) {
      bool emailValid =
          model.submitted && !model.emailValidator.isValid(model.email);
      bool passwordValid =
          model.submitted && !model.emailValidator.isValid(model.password);

      return [
        SignInLogo(),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white.withOpacity(0.8),
          ),
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              CustomTextField(
                autocorrect: true,
                controller: _emailController,
                errorText: 'Invalid email',
                hintText: 'test@nwc.com',
                labelText: 'Your Email',
                focusNode: _emailFocusNode,
                obscureText: false,
                keyboardType: TextInputType.emailAddress,
                callback: (email) => widget.bloc.updateEmail(email),
                onEditingComplete: () => _emailEditingComplete(model),
                action: TextInputAction.next,
                isValid: emailValid,
              ),
              CustomTextField(
                autocorrect: false,
                controller: _passwordController,
                errorText: 'Invalid password',
                labelText: 'Your Password',
                focusNode: _passwordFocusNode,
                obscureText: true,
                callback: (password) => widget.bloc.updatePassword(password),
                onEditingComplete: _submit,
                action: TextInputAction.done,
                isValid: passwordValid,
              ),
              GradientButton(
                title: model.PrimaryButtonText,
                callback: model.canSubmit ? _submit : null,
              ),
              TextButton(
                child: Text(
                  model.SecondaryButtonText,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 12.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                onPressed: () => _toggleFormType(model),
              ),
            ],
          ),
        )
      ];
    }

    return StreamBuilder<EmailSignInModel>(
      stream: widget.bloc.modelStream,
      initialData: EmailSignInModel(),
      builder: (context, snapshot) {
        final EmailSignInModel model = snapshot.data;

        return Column(
          children: _buildChildren(model),
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
        );
      },
    );
  }
}
