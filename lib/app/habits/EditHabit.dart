import 'package:flutter/material.dart';
import 'package:nerd_fit/app/home/TabItem.dart';
import 'package:nerd_fit/components/MarginBottom.dart';
import 'package:nerd_fit/components/PlatformExceptionAlert.dart';
import 'package:nerd_fit/components/SubmitButton.dart';
import 'package:nerd_fit/models/HabbitModel.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class EditHabit extends StatefulWidget {
  EditHabit({this.onSelectTab});
  final ValueChanged<TabItem> onSelectTab;

  @override
  State<EditHabit> createState() => _EditHabitState();
}

class _EditHabitState extends State<EditHabit> {
  final _form = GlobalKey<FormState>();
  final TextEditingController _habitNameController = TextEditingController();
  final TextEditingController _habitDescriptionController =
      TextEditingController();

  String dropdownValue = 'Daily';

  @override
  Widget build(BuildContext context) {
    // create new habit
    Future<void> _createHabit(BuildContext context) async {
      final database = await Provider.of<Database>(context, listen: false);

      // get current items from FB
      // final existingItems = await database.habitListItemStream().first;
      // final allTitles = existingItems.map((item) => item.title).toList();
      // final id = item?.id ?? documentIdFromCurrentDate();

      try {
        DateTime createdDate = DateTime.now();

        print(_habitDescriptionController.text);
        print(_habitNameController.text);
        await database.setHabitListItem(HabitModel(
          id: documentIdFromCurrentDate(),
          title: _habitNameController.text,
          createdAt:
              createdDate.subtract(Duration(days: 365)).toIso8601String(),
          description: _habitDescriptionController.text,
          occurance: dropdownValue,
          complete: false,
        ));

        widget.onSelectTab(TabItem.habits);
      } catch (e) {
        PlatformExceptionAlert(title: 'Habit Creation Failed', exception: e)
            .show(context);
      }
    }

    return Scaffold(
      body: Form(
        key: _form,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/bg--login.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 8,
                  offset: const Offset(0, 0),
                ),
              ],
            ),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  // ## habit name
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.check_circle_outline,
                        size: 14.0,
                      ),
                      Text(' Habit Name',
                          style: TextStyle(
                            color: Colors.black,
                          )),
                    ],
                  ),
                  MarginBottom(gap: 5.0),
                  TextFormField(
                    autocorrect: true,
                    controller: _habitNameController,
                    decoration: InputDecoration(
                      //errorText: passwordValid ? 'Invalid password' : null,
                      labelText: null,
                    ),
                    // focusNode: _passwordFocusNode,
                    obscureText: false,
                    // onSaved: (value) => _name = value,
                    // onEditingComplete: _submit,
                    // textInputAction: TextInputAction.done,
                  ),
                  MarginBottom(),
                  // ## occurance inputs
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.access_alarms_outlined,
                        size: 14.0,
                      ),
                      Text(' Frequency',
                          style: TextStyle(
                            color: Colors.black,
                          )),
                    ],
                  ),
                  MarginBottom(gap: 5.0),
                  Container(
                    width: double.infinity,
                    child: DropdownButton<String>(
                      value: dropdownValue,
                      icon: const Icon(Icons.arrow_downward),
                      iconSize: 24,
                      isExpanded: true,
                      elevation: 16,
                      // style: const TextStyle(color: Colors.deepPurple),
                      // underline: Container(
                      //   height: 2,
                      //   color: Colors.deepPurpleAccent,
                      // ),
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;
                        });
                      },
                      items: <String>[
                        'Daily',
                        'Weekly',
                        'Monthly',
                        'Before I Die'
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  MarginBottom(),
                  // ## Description inputs
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.chat_bubble_outline_rounded,
                        size: 14.0,
                      ),
                      Text(' Description',
                          style: TextStyle(
                            color: Colors.black,
                          )),
                    ],
                  ),
                  MarginBottom(gap: 5.0),
                  TextFormField(
                    autocorrect: true,
                    controller: _habitDescriptionController,
                    decoration: InputDecoration(
                      //errorText: passwordValid ? 'Invalid password' : null,
                      labelText: null,
                    ),
                    // focusNode: _passwordFocusNode,
                    obscureText: false,
                    // onChanged: (value) => _description = value,
                    // onEditingComplete: _submit,
                    // textInputAction: TextInputAction.done,
                  ),
                  MarginBottom(),

                  MarginBottom(),
                  SubmitButton(
                    title: 'Save Habit',
                    onPressed: () {
                      _createHabit(context);
                    },
                    radius: 300,
                    bg: Colors.pinkAccent[400],
                  ),
                  TextButton(
                      onPressed: () {
                        widget.onSelectTab(TabItem.habits);
                      },
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
