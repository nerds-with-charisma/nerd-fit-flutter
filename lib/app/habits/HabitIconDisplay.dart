import 'package:flutter/material.dart';
import 'package:nerd_fit/models/HabbitModel.dart';

class HabitIconDisplay extends StatefulWidget {
  const HabitIconDisplay({this.item});
  final HabitModel item;

  @override
  State<HabitIconDisplay> createState() => _HabitIconDisplayState();
}

class _HabitIconDisplayState extends State<HabitIconDisplay> {
  Icon _setIcon(occurance) {
    if (occurance == 'Weekly') return Icon(Icons.calendar_view_week_outlined);
    if (occurance == 'Monthly') return Icon(Icons.calendar_view_month_outlined);
    if (occurance == 'Before I Die')
      return Icon(Icons.sentiment_very_dissatisfied_outlined);

    //default
    return Icon(Icons.calendar_today_outlined);
  }

  @override
  Widget build(BuildContext context) {
    Icon icon = _setIcon(widget.item.occurance);

    return icon;
  }
}
