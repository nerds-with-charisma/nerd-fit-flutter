import 'package:flutter/material.dart';
import 'package:nerd_fit/app/habits/UpdateHabitBottomSheet.dart';
import 'package:nerd_fit/app/home/TabItem.dart';
import 'package:nerd_fit/components/EmptyShoppingLIst.dart';
import 'package:nerd_fit/models/HabbitModel.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class HabitListView extends StatelessWidget {
  HabitListView({this.onSelectTab});
  final ValueChanged<TabItem> onSelectTab;

  @override
  Widget build(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);

    return StreamBuilder<List<HabitModel>>(
      stream: database.habitListItemStream(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final items = snapshot.data;

          if (items.isNotEmpty) {
            final children = items
                .map(
                  (item) => UpdateHabitBottomSheet(
                    onSelectTab: onSelectTab,
                    item: item,
                  ),
                )
                .toList();

            // check if all items are completed
            var contain = items.where((element) => element.complete == true);

            return Expanded(
              child: Column(
                children: [
                  (contain.length >= items.length)
                      ? Container(
                          // width: double.infinity,
                          // padding: EdgeInsets.all(10),
                          // color: Colors.grey[100],
                          child: Text(
                            //'Good job! You\'re done for today!',
                            '',
                            // textAlign: TextAlign.center,
                          ),
                        )
                      : SizedBox(
                          height: 0,
                        ),
                  Expanded(
                    child: ListView(
                      children: children,
                    ),
                  ),
                ],
              ),
            );
          } else {
            return EmptyShoppingList();
          }
        }
        if (snapshot.hasError) {
          return Center(child: Text('Some error occurred'));
        }

        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
