import 'package:flutter/material.dart';

class Streak extends StatelessWidget {
  Streak({this.streak});
  final int streak;

  @override
  Widget build(BuildContext context) {
    String _calculateStreakIcon() {
      if (streak < 5) return '🤗 Noob';
      if (streak >= 5 && streak < 10) return '🙂 Amature';
      if (streak >= 10 && streak < 50) return '😀 Pro';
      if (streak >= 50 && streak < 100) return '🥷 Ninja';
      if (streak >= 100) return '🔥 Jedi';
      return '';
    }

    Color _calculateColor() {
      if (streak < 5) return Colors.purple;
      if (streak >= 5 && streak < 10) return Colors.blueAccent;
      if (streak >= 10 && streak < 50) return Colors.orangeAccent;
      if (streak >= 50 && streak < 100) return Colors.redAccent;
      if (streak >= 100) return Colors.pinkAccent[400];
      return Colors.blueGrey;
    }

    return Text(
      _calculateStreakIcon(),
      style: TextStyle(
        color: _calculateColor(),
      ),
    );
  }
}
