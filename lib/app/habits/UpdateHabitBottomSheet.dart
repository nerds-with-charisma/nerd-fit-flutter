import 'package:flutter/material.dart';
import 'package:nerd_fit/app/habits/HabitItemDisplay.dart';
import 'package:nerd_fit/app/home/TabItem.dart';
import 'package:nerd_fit/components/MarginBottom.dart';
import 'package:nerd_fit/components/PlatformExceptionAlert.dart';
import 'package:nerd_fit/components/SubmitButton.dart';
import 'package:nerd_fit/models/HabbitModel.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class UpdateHabitBottomSheet extends StatefulWidget {
  UpdateHabitBottomSheet({this.item, this.onSelectTab});
  final HabitModel item;
  final ValueChanged<TabItem> onSelectTab;

  @override
  State<UpdateHabitBottomSheet> createState() => _UpdateHabitBottomSheetState();
}

class _UpdateHabitBottomSheetState extends State<UpdateHabitBottomSheet> {
  final _form = GlobalKey<FormState>();
  final TextEditingController _habitNameController = TextEditingController();

  final TextEditingController _habitDescriptionController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    String dropdownValue = 'Daily';

    setState(() {
      dropdownValue = widget.item.occurance;
    });

// init input values
    _habitNameController.value =
        _habitNameController.value.copyWith(text: widget.item.title);

    _habitDescriptionController.value = _habitDescriptionController.value
        .copyWith(text: widget.item.description);

    Future<void> _updateHabit() async {
      final database = await Provider.of<Database>(context, listen: false);

      try {
        print(_habitDescriptionController.text);
        print(_habitNameController.text);
        await database.setHabitListItem(HabitModel(
          id: widget.item.id,
          title: _habitNameController.text,
          createdAt: widget.item.createdAt,
          description: _habitDescriptionController.text,
          occurance: dropdownValue,
          complete: widget.item.complete,
          streak: widget.item.streak + 1,
        ));

        widget.onSelectTab(TabItem.habits);
      } catch (e) {
        PlatformExceptionAlert(title: 'Habit Edit Failed', exception: e)
            .show(context);
      }
    }

    return GestureDetector(
      onLongPress: () {
        // left off here, need to pass item to edit screen
        showModalBottomSheet<void>(
          isScrollControlled: true,
          context: context,
          builder: (BuildContext context) {
            return Container(
              height: 650,
              color: Colors.white,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      child: Form(
                        key: _form,
                        child: Container(
                          padding: EdgeInsets.all(8),
                          child: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                SingleChildScrollView(
                                  child: Column(
                                    children: <Widget>[
                                      // ## habit name
                                      Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.check_circle_outline,
                                            size: 14.0,
                                          ),
                                          Text(' Habit Name',
                                              style: TextStyle(
                                                color: Colors.black,
                                              )),
                                        ],
                                      ),
                                      MarginBottom(gap: 5.0),
                                      SizedBox(
                                        height: 35,
                                        child: TextFormField(
                                          autocorrect: true,
                                          autofocus: true,
                                          controller: _habitNameController,
                                          decoration: InputDecoration(
                                            //errorText: passwordValid ? 'Invalid password' : null,
                                            labelText: null,
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    horizontal: 15,
                                                    vertical: 3),
                                          ),
                                          // focusNode: _passwordFocusNode,
                                          obscureText: false,
                                          // onSaved: (value) => _name = value,
                                          // onEditingComplete: _submit,
                                          // textInputAction: TextInputAction.done,
                                        ),
                                      ),
                                      MarginBottom(),
                                      // ## Description inputs
                                      Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.chat_bubble_outline_rounded,
                                            size: 14.0,
                                          ),
                                          Text(' Description',
                                              style: TextStyle(
                                                color: Colors.black,
                                              )),
                                        ],
                                      ),
                                      MarginBottom(gap: 5.0),
                                      SizedBox(
                                        height: 35,
                                        child: TextFormField(
                                          autocorrect: true,
                                          controller:
                                              _habitDescriptionController
                                                ..text =
                                                    widget.item.description,
                                          decoration: InputDecoration(
                                            //errorText: passwordValid ? 'Invalid password' : null,
                                            labelText: null,
                                          ),
                                          // focusNode: _passwordFocusNode,
                                          obscureText: false,
                                          // onChanged: (value) => _description = value,
                                          // onEditingComplete: _submit,
                                          // textInputAction: TextInputAction.done,
                                        ),
                                      ),
                                      MarginBottom(),
                                      // ## occurance inputs
                                      Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.access_alarms_outlined,
                                            size: 14.0,
                                          ),
                                          Text(' Frequency',
                                              style: TextStyle(
                                                color: Colors.black,
                                              )),
                                        ],
                                      ),
                                      MarginBottom(gap: 5.0),
                                      DropdownButton<String>(
                                        value: dropdownValue,
                                        icon: const Icon(Icons.arrow_downward),
                                        iconSize: 24,
                                        isExpanded: true,
                                        elevation: 16,
                                        // style: const TextStyle(color: Colors.deepPurple),
                                        // underline: Container(
                                        //   height: 2,
                                        //   color: Colors.deepPurpleAccent,
                                        // ),
                                        onChanged: (String newValue) {
                                          setState(() {
                                            dropdownValue = newValue;
                                          });
                                        },
                                        items: <String>[
                                          'Daily',
                                          'Weekly',
                                          'Monthly',
                                          'Before I Die'
                                        ].map<DropdownMenuItem<String>>(
                                            (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                      ),
                                      MarginBottom(),

                                      SizedBox(
                                        height: 45,
                                        child: SubmitButton(
                                          title: 'Save Habit',
                                          onPressed: () {
                                            _updateHabit();
                                          },
                                          radius: 300,
                                          bg: Colors.pinkAccent[400],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 35,
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: Text(
                                            'Cancel',
                                            style: TextStyle(
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
      child: HabitItemDisplay(
        item: widget.item,
      ),
    );
  }
}
