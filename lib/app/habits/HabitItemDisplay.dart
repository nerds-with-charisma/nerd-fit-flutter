import 'package:flutter/material.dart';
import 'package:nerd_fit/app/habits/HabitIconDisplay.dart';
import 'package:nerd_fit/models/HabbitModel.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class HabitItemDisplay extends StatelessWidget {
  HabitItemDisplay({@required this.item});
  final HabitModel item;

  @override
  Widget build(BuildContext context) {
    Color _calculateColor() {
      print(item);
      print(item.streak);
      if (item.streak == null) return Colors.blueGrey;

      if (item.streak < 5) return Colors.purple[300];
      if (item.streak < 10) return Colors.blueAccent;
      if (item.streak < 50) return Colors.orange;
      if (item.streak < 100) return Colors.redAccent[400];
      if (item.streak >= 100) return Colors.pinkAccent[400];
      return Colors.blueGrey;
    }

    bool _isCompleted = false;
    int _streak = item.streak != null ? item.streak : 0;

    // check if completed within the window of the occurance
    bool _checkCompleted() {
      _isCompleted = item.complete; // set it by default, prob won't use it

      final now = DateTime.now(); // current date
      final today = DateTime(now.year, now.month, now.day);
      final dateToCheck = DateTime.parse(item.createdAt);
      final aDate =
          DateTime(dateToCheck.year, dateToCheck.month, dateToCheck.day);

      // check our daily checks
      if (item.occurance == 'Daily') {
        if (aDate == today) {
          _isCompleted = true; // completed today
        } else {
          _isCompleted = false; // not completed today
        }
      } else if (item.occurance == 'Weekly') {
        var d = DateTime.now();
        var weekDay = d.weekday;
        var firstDayOfWeek = d.subtract(Duration(days: weekDay));

        if (aDate.isAfter(firstDayOfWeek)) {
          _isCompleted = true;
        } else {
          _isCompleted = false;
        }
        print('weekly');
      } else if (item.occurance == 'Monthly') {
        var d = DateTime.now();
        var month = d.month;
        var firstDayOfMonth = d.subtract(Duration(days: month));

        if (aDate.isAfter(firstDayOfMonth)) {
          _isCompleted = true;
        } else {
          _isCompleted = false;
        }
        print('monthly');
      } else if (item.occurance == 'Before I Die') {
        _isCompleted = item.complete;
      }

      return _isCompleted;
    }

    return Dismissible(
      background: Container(
        alignment: Alignment.centerRight,
        color: Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
        padding: EdgeInsets.only(right: 20),
      ),
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to delete this habbit?'),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.of(ctx).pop(false);
                  },
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  )),
              TextButton(
                  onPressed: () {
                    Navigator.of(ctx).pop(true);
                  },
                  child: Text(
                    'Confirm Delete',
                    style: TextStyle(
                      color: Colors.redAccent,
                    ),
                  ))
            ],
          ),
        );
      },
      direction: DismissDirection.endToStart,
      onDismissed: (direction) async {
        try {
          final database = Provider.of<Database>(context, listen: false);
          await database.deleteHabitListItem(item);
        } catch (e) {
          print(e);
        }
      },
      key: UniqueKey(),
      child: Container(
        decoration: new BoxDecoration(
            border:
                new Border(bottom: new BorderSide(color: Colors.grey[200]))),
        child: ListTile(
          dense: true,
          contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(item.description),
              // sStreak(streak: _streak),
            ],
          ),
          title: Text(
            item.title,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 16.0,
              color: Colors.black,
            ),
          ),
          trailing: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: _calculateColor().withOpacity(0.4),
                  spreadRadius: 0,
                  blurRadius: 3,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
              borderRadius: BorderRadius.circular(300),
            ),
            child: IconButton(
              color: Colors.blueGrey[400],
              icon: HabitIconDisplay(item: item),
              onPressed: () {},
            ),
          ),
          leading: IconButton(
            color: _checkCompleted() == true
                ? Colors.tealAccent[400]
                : Colors.pinkAccent[400],
            icon: Icon(_checkCompleted() == true
                ? Icons.check_circle_outline_outlined
                : Icons.cancel),
            onPressed: () async {
              final database = Provider.of<Database>(context, listen: false);
              await database.setHabitListItem(HabitModel(
                id: item.id,
                title: item.title,
                streak: _checkCompleted() == true ? _streak - 1 : _streak + 1,
                complete: !_checkCompleted(),
                createdAt: _checkCompleted() == true
                    ? DateTime.now()
                        .subtract(Duration(days: 365))
                        .toIso8601String()
                    : DateTime.now()
                        .toIso8601String(), // set to updated now, we will check this later to see if completed within the  occurance window
                description: item.description,
                occurance: item.occurance,
              ));
            },
          ),
        ),
      ),
    );
  }
}
