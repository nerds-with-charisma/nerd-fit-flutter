import 'package:flutter/material.dart';
import 'package:nerd_fit/app/dimensions/DimensionTitleButton.dart';
import 'package:nerd_fit/components/MarginBottom.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class DimensionCard extends StatefulWidget {
  DimensionCard({this.title});
  final String title;

  @override
  State<DimensionCard> createState() => _DimensionCardState();
}

class _DimensionCardState extends State<DimensionCard> {
  var userProfileData;
  bool _hasLoaded = false;
  String _value;
  FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    Future<void> _init() async {
      if (_hasLoaded != true) {
        final database = await Provider.of<Database>(context, listen: false);
        var userData = await database.getUserDataStream().first;

        if (userData.length > 0) {
          setState(() {
            userProfileData = userData[0];
            _hasLoaded = true;
          });

          // calculate which value to use
          var u = userData[0];
          var t = widget.title.toLowerCase();
          if (t == 'weight') _value = '${u.weight != null ? u.weight : 0}';
          if (t == 'neck') _value = '${u.dimsNeck != null ? u.dimsNeck : 0}';
          if (t == 'chest') _value = '${u.dimsChest != null ? u.dimsChest : 0}';
          if (t == 'bicep') _value = '${u.dimsBicep != null ? u.dimsBicep : 0}';
          if (t == 'waist') _value = '${u.dimsWaist != null ? u.dimsWaist : 0}';
          if (t == 'thigh') _value = '${u.dimsThigh != null ? u.dimsThigh : 0}';
        }
      }
    }

    _init();

    Future<void> _updateDimInDb() async {
      // update ORM on letting go
      final db = await Provider.of<Database>(context, listen: false);
      var t = widget.title.toLowerCase();

      // set new dimensions
      if (t == 'weight') {
        print('****weight: ${userProfileData.originalWeight}');
        await db.updateUserData({'weight': double.parse(_value)});
        if (userProfileData.originalWeight == null)
          await db.updateUserData({'originalWeight': double.parse(_value)});
      }

      if (t == 'neck') {
        await db.updateUserData({'dimsNeck': double.parse(_value)});
        if (userProfileData.originalDimsNeck == null)
          await db.updateUserData({'originalDimsNeck': double.parse(_value)});
      }

      if (t == 'chest') {
        await db.updateUserData({'dimsChest': double.parse(_value)});
        if (userProfileData.originalDimsChest == null)
          await db.updateUserData({'originalDimsChest': double.parse(_value)});
      }

      if (t == 'bicep') {
        await db.updateUserData({'dimsBicep': double.parse(_value)});
        if (userProfileData.originalDimsBicep == null)
          await db.updateUserData({'originalDimsBicep': double.parse(_value)});
      }
      if (t == 'waist') {
        await db.updateUserData({'dimsWaist': double.parse(_value)});
        if (userProfileData.originalDimsWaist == null)
          await db.updateUserData({'originalDimsWaist': double.parse(_value)});
      }
      if (t == 'thigh') {
        await db.updateUserData({'dimsThigh': double.parse(_value)});
        if (userProfileData.originalDimsThigh == null)
          await db.updateUserData({'originalDimsThigh': double.parse(_value)});
      }
    }

    focusNode.addListener(() {
      if (_value != '0') {
        _updateDimInDb();
      }
    });

    if (userProfileData == null) return SizedBox();

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26.withOpacity(0.1),
              blurRadius: 8,
              offset: const Offset(0, 0),
            ),
          ],
        ),
        width: double.infinity,
        padding: EdgeInsets.all(8),
        child: Column(
          children: [
            MarginBottom(gap: 12),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                DimensionTitleButton(
                  title: widget.title,
                  current: 10,
                  starting: 2,
                ),
                Container(
                  width: 100,
                  child: TextFormField(
                    focusNode: focusNode,
                    initialValue: _value,
                    onChanged: (String value) {
                      setState(() {
                        _value = value;
                      });
                    },
                  ),
                )
              ],
            ),
            MarginBottom(gap: 12),
          ],
        ),
      ),
    );
  }
}
