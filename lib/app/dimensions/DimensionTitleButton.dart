import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class dimValuesTimeline {
  final String dimension;
  final double dimValue;

  dimValuesTimeline({
    @required this.dimension,
    @required this.dimValue,
  });
}

class DimensionTitleButton extends StatelessWidget {
  DimensionTitleButton({this.title, this.starting, this.current});
  final String title;
  final starting;
  final current;

  @override
  Widget build(BuildContext context) {
    final List<charts.Series<dimValuesTimeline, String>> timeline = [
      charts.Series(
        id: "Current vs. Original",
        data: [
          dimValuesTimeline(
              dimension: "Starting vs. Current", dimValue: current.toDouble()),
        ],
        colorFn: (_, __) => charts.MaterialPalette.pink.shadeDefault,
        domainFn: (dimValuesTimeline timeline, _) => timeline.dimension,
        measureFn: (dimValuesTimeline timeline, _) => timeline.dimValue,
      ),
      charts.Series(
        id: "Current vs. Original",
        data: [
          dimValuesTimeline(
              dimension: "Starting vs. Current",
              dimValue: starting.toDouble() * 2),
        ],
        colorFn: (_, __) => charts.MaterialPalette.gray.shade300,
        domainFn: (dimValuesTimeline timeline, _) => timeline.dimension,
        measureFn: (dimValuesTimeline timeline, _) => timeline.dimValue,
      )
    ];

    return GestureDetector(
      onTap: () {
        showModalBottomSheet<void>(
          isScrollControlled: true,
          context: context,
          builder: (BuildContext context) {
            return Container(
              height: 200,
              padding: EdgeInsets.all(20),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        "${title} Dimensions",
                      ),
                      Expanded(
                        child: charts.BarChart(
                          timeline,
                          animate: true,
                          barGroupingType: charts.BarGroupingType.stacked,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
      child: Row(
        children: [
          Text(
            '${title}',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Icon(
            Icons.timeline_rounded,
            size: 21.0,
            color: Colors.pinkAccent[400],
          ),
        ],
      ),
    );
  }
}
