import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nerd_fit/components/MarginBottom.dart';

class TipsTabs extends StatefulWidget {
  @override
  State<TipsTabs> createState() => _TipsTabsState();
}

class _TipsTabsState extends State<TipsTabs> {
  var _tips;
  @override
  Widget build(BuildContext context) {
    Future<void> _getTips(path) async {
      String tips = await rootBundle.loadString(path);
      final data = await json.decode(tips);
      setState(() => _tips = data['articles']);
    }

// todo left off here, need to get each json on click
    _getTips('assets/Tips/Running/articles.json');

    if (_tips == null) return Text('Loading...');

    List _tabs = [
      {
        'icon': Icon(Icons.run_circle),
        'label': 'Running',
        'widget': Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: _tips.map<Widget>((a) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.network(a['coverImg']),
                  MarginBottom(gap: 8.0),
                  Text(
                    a['title'],
                    style: TextStyle(
                      // fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                  MarginBottom(gap: 5.0),
                  Text(
                    a['shortDesc'],
                    style: TextStyle(color: Colors.grey[600]),
                  ),
                  MarginBottom(gap: 30.0),
                ],
              );
            }).toList(),
          ),
        ),
      },
      {
        'icon': Icon(Icons.food_bank),
        'label': 'Diet',
        'widget': Text('Diet'),
      },
      {
        'icon': Icon(Icons.receipt),
        'label': 'Recipes',
        'widget': Text('Recipes'),
      },
      {
        'icon': Icon(Icons.food_bank),
        'label': 'Hobbies',
        'widget': Text('Hobbies'),
      },
      {
        'icon': Icon(Icons.food_bank),
        'label': 'Exercises',
        'widget': Text('Exercises'),
      },
      {
        'icon': Icon(Icons.food_bank),
        'label': 'Body Maintenence',
        'widget': Text('Body Maintenence'),
      },
    ];

    return Container(
      child: DefaultTabController(
        length: _tabs.length,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            flexibleSpace: SafeArea(
              child: TabBar(
                indicatorColor: Colors.pinkAccent[400],
                labelColor: Colors.black,
                unselectedLabelColor: Colors.grey,
                isScrollable: true,
                tabs: _tabs.map((tabItem) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Tab(
                      // icon: tabItem['icon'],
                      text: tabItem['label'],
                    ),
                  );
                }).toList(),
              ),
            ),
            backgroundColor: Colors.white,
            // title: Text('Fitness Tips'),
          ),
          body: Container(
            color: Colors.white,
            child: SingleChildScrollView(
              child: Container(
                width: double.infinity,
                height: 350.0 * _tips.length,
                constraints: BoxConstraints(
                  maxHeight: double.infinity,
                ),
                child: TabBarView(
                  children: List<Widget>.generate(_tabs.length, (int index) {
                    return Container(
                      width: double.infinity,
                      child: _tabs[index]['widget'],
                    );
                  }),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
