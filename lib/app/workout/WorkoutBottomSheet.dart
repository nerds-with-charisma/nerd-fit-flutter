import 'package:flutter/material.dart';
import 'package:nerd_fit/app/workout/PopulateWorkoutData.dart';
import 'package:nerd_fit/components/MarginBottom.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class WorkoutBottomSheet extends StatelessWidget {
  WorkoutBottomSheet({
    this.exercise,
    this.weight,
  });
  final dynamic exercise;
  final String weight;

  @override
  Widget build(BuildContext context) {
    YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId: getData(exercise['dataId'])['video'],
      params: YoutubePlayerParams(
        startAt: Duration(seconds: 0),
        showControls: true,
        showFullscreenButton: true,
      ),
    );

    return IconButton(
      icon: Icon(Icons.info_outline_rounded),
      onPressed: () {
        showModalBottomSheet<void>(
          isScrollControlled: true,
          context: context,
          builder: (BuildContext context) {
            return SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20),
                height: 650,
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    getData(exercise['dataId'])['video'] != null
                        ? YoutubePlayerIFrame(
                            controller: _controller,
                            aspectRatio: 16 / 9,
                          )
                        : SizedBox(),
                    MarginBottom(),
                    // ## Name
                    Text(
                      exercise['name'],
                      style: TextStyle(
                        fontSize: 22,
                      ),
                    ),
                    // ## Exercise
                    Text(
                      exercise['exercise'],
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Text(
                      'Alternatives: ${getData(exercise['dataId'])['alternatives']}',
                      style: TextStyle(
                        fontSize: 10,
                      ),
                    ),
                    Divider(),
                    // ## Specs Info
                    Row(
                      children: [
                        // ## Sets
                        Container(
                          padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
                          child: Column(
                            children: [
                              Text(
                                exercise['sets'] > 0
                                    ? '${exercise['sets']}'
                                    : 'N/A',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 21,
                                ),
                              ),
                              Text(
                                'Set',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ),
                        ),

                        // ## Reps
                        Column(
                          children: [
                            Text(
                              '${exercise['reps']}',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 21,
                              ),
                            ),
                            Text(
                              'Reps',
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                        // ## Weight
                        Expanded(
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Column(
                              children: [
                                exercise['weightPercentage'] != null
                                    ? Text(
                                        weight.toString(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 21,
                                        ),
                                      )
                                    : Text(exercise['timeInMin'].toString(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 21,
                                        )),
                                Text(
                                  exercise['weightPercentage'] != null
                                      ? 'Weight'
                                      : 'Minutes',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Divider(),
                    // ## Description
                    Text(
                      getData(exercise['dataId'])['description'],
                    ),

                    MarginBottom(),
                    // ## Read  more links
                    ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.black, // background
                        onPrimary: Colors.white, // foreground
                      ),
                      icon: Icon(
                        Icons.open_in_browser,
                        color: Colors.white,
                        size: 24.0,
                      ),
                      label: Text('Learn More'),
                      onPressed: () async {
                        if (await canLaunch(getData(exercise['dataId'])['url']))
                          await launch(getData(exercise['dataId'])['url']);
                        else
                          throw "Could not launch url";
                      },
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
