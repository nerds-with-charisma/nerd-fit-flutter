import 'package:flutter/material.dart';
import 'package:nerd_fit/app/workout/WorkoutBottomSheet.dart';
import 'package:nerd_fit/components/MarginBottom.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class WorkoutCard extends StatefulWidget {
  WorkoutCard({
    this.exercise,
    this.calcWeight,
    this.updateSuccess,
    this.showUn,
    this.showSuccess,
    this.reload,
  });

  final dynamic exercise;
  final Function calcWeight;
  final Function updateSuccess;
  final bool showUn;
  final bool showSuccess;
  final Function reload;

  @override
  _WorkoutCardState createState() => _WorkoutCardState();
}

class _WorkoutCardState extends State<WorkoutCard> {
  bool _showSuccess = true;
  bool _showUn = true;
  double _label = 0;
  String _exerciseName = '';

  @override
  Widget build(BuildContext context) {
    Future<void> _updateOrmInDb(value) async {
      // update ORM on letting go
      final db = await Provider.of<Database>(context, listen: false);
      String ex = widget.exercise['exercise'].toLowerCase();
      if (ex == 'squat') await db.updateUserData({'squatORM': value});
      if (ex == 'bench') await db.updateUserData({'benchORM': value});
      if (ex == 'deadlift') await db.updateUserData({'deadliftORM': value});
      if (ex == 'calf-raises')
        await db.updateUserData({'calfRaisesORM': value});
      if (ex == 'glute-bridge')
        await db.updateUserData({'gluteBridgeORM': value});
      if (ex == 'curl') await db.updateUserData({'curlORM': value});
      if (ex == 'row') await db.updateUserData({'rowORM': value});
      if (ex == 'uppercuts') await db.updateUserData({'uppercutORM': value});
      if (ex == 'press') await db.updateUserData({'pressORM': value});
      if (ex == 'close') await db.updateUserData({'closeORM': value});
      if (ex == 'rdl') await db.updateUserData({'rdlORM': value});
      if (ex == 'incline') await db.updateUserData({'indlineORM': value});
      if (ex == 'lat') await db.updateUserData({'latORM': value});
      if (ex == 'shrug') await db.updateUserData({'shrugORM': value});
    }

    return GestureDetector(
      onLongPress: () async {
        final database = Provider.of<Database>(context, listen: false);
        final userData = await database.getUserDataStream().first;

        setState(() => _exerciseName = widget.exercise['name']);

        if (widget.exercise['name'] == 'Squat')
          setState(() => _label = userData[0].squatORM);
        if (widget.exercise['name'] == 'Deadlift')
          setState(() => _label = userData[0].deadliftORM);
        if (widget.exercise['name'] == 'Bench')
          setState(() => _label = userData[0].benchORM);
        if (widget.exercise['name'] == 'Calf-Raises')
          setState(() => _label = userData[0].calfRaisesORM);
        if (widget.exercise['name'] == 'Curl')
          setState(() => _label = userData[0].curlORM);
        if (widget.exercise['name'] == 'Glute-Bridge')
          setState(() => _label = userData[0].gluteBridgeORM);
        if (widget.exercise['name'] == 'Row')
          setState(() => _label = userData[0].rowORM);
        if (widget.exercise['name'] == 'Uppercuts')
          setState(() => _label = userData[0].uppercutORM);
        if (widget.exercise['name'] == 'Shoulder Press')
          setState(() => _label = userData[0].pressORM);
        if (widget.exercise['name'] == 'Close-Grip-Bench')
          setState(() => _label = userData[0].closeORM);
        if (widget.exercise['name'] == 'Romanian-Dead-Lift')
          setState(() => _label = userData[0].rdlORM);
        if (widget.exercise['name'] == 'Incline-Bench')
          setState(() => _label = userData[0].inclineORM);
        if (widget.exercise['name'] == 'Lat-Raises')
          setState(() => _label = userData[0].latORM);
        if (widget.exercise['name'] == 'Shrugs')
          setState(() => _label = userData[0].shrugORM);

        if (widget.exercise['sets'] > 0 && widget.exercise['oneRepMax'] > 0) {
          showModalBottomSheet<void>(
            isScrollControlled: true,
            context: context,
            builder: (BuildContext context) {
              return SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(30),
                  height: 150,
                  color: Colors.white,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Adjust your ${_exerciseName} one rep max: ${_label}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        'Will reflect next time you visit the workout page.',
                        style: TextStyle(
                          color: Colors.blueGrey,
                          fontSize: 12,
                        ),
                      ),
                      StatefulBuilder(builder: (context, setState) {
                        return Slider(
                          value: _label,
                          activeColor: Colors.black,
                          min: 0,
                          max: 400,
                          divisions: 355,
                          label: _label.toString(),
                          onChangeEnd: (double value) async {
                            await _updateOrmInDb(value.roundToDouble());

                            widget.reload();

                            Navigator.pop(context, 'refresh');
                          },
                          onChanged: (double value) async {
                            setState(() => _label = value.roundToDouble());
                          },
                        );
                      })
                    ],
                  ),
                ),
              );
            },
          );
        }
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MarginBottom(gap: 10),
            Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(2)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 5,
                    offset: Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        // ## Title
                        Padding(
                          padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                widget.exercise['name'],
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              (widget.exercise['dataId'] != null)
                                  ? WorkoutBottomSheet(
                                      exercise: widget.exercise,
                                      weight:
                                          widget.exercise['weightPercentage'] !=
                                                  null
                                              ? widget
                                                  .calcWeight(widget.exercise)
                                              : null)
                                  : Text(''),
                            ],
                          ),
                        ),

                        Divider(),

                        // ## Info
                        Row(
                          children: [
                            // ## Sets
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
                              child: Column(
                                children: [
                                  Text(
                                    (widget.exercise['sets'] != null &&
                                            widget.exercise['sets'] > 0)
                                        ? '${widget.exercise['sets']}'
                                        : 'N/A',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 24,
                                    ),
                                  ),
                                  Text(
                                    'Set',
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            // ## Reps
                            Column(
                              children: [
                                Text(
                                  '${widget.exercise['reps']}',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 24,
                                  ),
                                ),
                                Text(
                                  'Reps',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                  ),
                                ),
                              ],
                            ),
                            // ## Weight
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Column(
                                  children: [
                                    widget.exercise['weightPercentage'] != null
                                        ? Text(
                                            widget.calcWeight(widget.exercise),
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 24,
                                            ),
                                          )
                                        : Text(
                                            widget.exercise['timeInMin']
                                                .toString(),
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 24,
                                            )),
                                    Text(
                                      widget.exercise['weightPercentage'] !=
                                              null
                                          ? 'Weight'
                                          : 'Minutes',
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              padding: EdgeInsets.all(10),
                              child: (widget.exercise['sets'] != null &&
                                      widget.exercise['sets'] > 0 &&
                                      widget.exercise['weightPercentage'] !=
                                          null &&
                                      widget.exercise['isFinalSet'] == true)
                                  ? Column(
                                      children: [
                                        _showUn == true
                                            ? IconButton(
                                                icon: const Icon(
                                                  Icons.remove_circle_outline,
                                                  color: Colors.black,
                                                ),
                                                tooltip: 'Unsucessful attempt',
                                                onPressed: () {
                                                  setState(() {
                                                    _showSuccess = true;
                                                    _showUn = false;
                                                  });
                                                  widget.updateSuccess(false,
                                                      widget.exercise, context);
                                                },
                                              )
                                            : IconButton(
                                                icon: const Icon(
                                                  Icons.remove_circle_outline,
                                                  color: Colors.redAccent,
                                                ),
                                                tooltip: 'Unsucessful attempt',
                                                onPressed: () {},
                                              ),
                                        Text('Drop It',
                                            style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.black87,
                                            )),
                                      ],
                                    )
                                  : SizedBox(),
                            ),
                            (widget.exercise['sets'] > 0 &&
                                    widget.exercise['weightPercentage'] !=
                                        null &&
                                    widget.exercise['isFinalSet'] == true)
                                ? Column(
                                    children: [
                                      _showSuccess == true
                                          ? IconButton(
                                              icon: Icon(
                                                Icons.add_circle_outline,
                                                color: widget.exercise[
                                                            'wasSuccessful'] ==
                                                        true
                                                    ? Colors.tealAccent[400]
                                                    : Colors.black,
                                              ),
                                              tooltip: 'Successful attempt',
                                              onPressed: () {
                                                setState(() {
                                                  _showSuccess = false;
                                                  _showUn = true;
                                                });
                                                widget.updateSuccess(true,
                                                    widget.exercise, context);
                                              })
                                          : IconButton(
                                              icon: Icon(
                                                  Icons.add_circle_outline,
                                                  color:
                                                      Colors.tealAccent[400]),
                                              tooltip: 'Successful attempt',
                                              onPressed: () {},
                                            ),
                                      Text(
                                        'Up it',
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: widget.exercise[
                                                      'wasSuccessful'] ==
                                                  true
                                              ? Colors.tealAccent[400]
                                              : Colors.black87,
                                        ),
                                      )
                                    ],
                                  )
                                : SizedBox(),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
