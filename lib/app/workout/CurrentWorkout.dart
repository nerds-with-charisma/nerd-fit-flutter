import 'package:flutter/material.dart';
import 'package:nerd_fit/app/workout/WorkoutCard.dart';
import 'package:nerd_fit/components/SubmitButton.dart';
import 'package:nerd_fit/providers/database.dart';
import 'package:provider/provider.dart';

class CurrentWorkout extends StatefulWidget {
  CurrentWorkout({this.userData, this.item, this.completeWorkout, this.reload});

  final dynamic userData;
  final dynamic item;
  final Function completeWorkout;
  final Function reload;

  @override
  State<CurrentWorkout> createState() => _CurrentWorkoutState();
}

class _CurrentWorkoutState extends State<CurrentWorkout> {
  Future<void> _updateSuccess(success, exercise, context) async {
    final database = await Provider.of<Database>(context, listen: false);
    final userProfileData = await database.getUserDataStream().first;

    if (exercise['weightPercentage'] != null) {
      //dynamic exerciseMute = exercise['wasSuccessful'] = success;

      String eName = exercise['exercise'].toLowerCase();
      dynamic upd = userProfileData[0];
      double incrementer = 5.0;
      if (success != true) incrementer = -5.0;

      if (eName == 'squat') {
        double sORM = (upd.squatORM + incrementer).toDouble();
        await database.updateUserData({'squatORM': sORM}); // update db
      }

      if (eName == 'bench') {
        double bORM = (upd.benchORM + incrementer).toDouble();
        await database.updateUserData({'benchORM': bORM}); // update db
      }

      if (eName == 'deadlift') {
        double dORM = (upd.deadliftORM + incrementer).toDouble();
        await database.updateUserData({'deadliftORM': dORM}); // update db
      }

      if (eName == 'calf-raises') {
        double cORM = (upd.calfRaisesORM + incrementer).toDouble();
        await database.updateUserData({'calfRaisesORM': cORM}); // update db
      }

      if (eName == 'glute-bridge') {
        double gORM = (upd.gluteBridgeORM + incrementer).toDouble();
        await database.updateUserData({'gluteBridgeORM': gORM}); // update db
      }

      if (eName == 'curl') {
        double curlORM = (upd.curlORM + incrementer).toDouble();
        await database.updateUserData({'curlORM': curlORM}); // update db
      }

      if (eName == 'row') {
        double rORM = (upd.rowORM + incrementer).toDouble();
        await database.updateUserData({'rowORM': rORM}); // update db
      }

      if (eName == 'uppercuts') {
        double uORM = (upd.uppercutORM + incrementer).toDouble();
        await database.updateUserData({'uppercutORM': uORM}); // update db
      }

      if (eName == 'shoulder-press') {
        double pORM = (upd.pressORM + incrementer).toDouble();
        await database.updateUserData({'pressORM': pORM}); // update db
      }

      if (eName == 'close-grip-bench') {
        double cgbORM = (upd.closeORM + incrementer).toDouble();
        await database.updateUserData({'closeORM': cgbORM}); // update db
      }

      if (eName == 'romanian-dead-lift') {
        double rdlORM = (upd.rdlORM + incrementer).toDouble();
        await database.updateUserData({'rdlORM': rdlORM}); // update db
      }

      if (eName == 'incline-bench') {
        double iORM = (upd.inclineORM + incrementer).toDouble();
        await database.updateUserData({'inclineORM': iORM}); // update db
      }

      if (eName == 'lat-raises') {
        double lORM = (upd.latORM + incrementer).toDouble();
        await database.updateUserData({'latORM': lORM}); // update db
      }

      if (eName == 'shrug') {
        double sORM = (upd.shrugORM + incrementer).toDouble();
        await database.updateUserData({'shrugORM': sORM}); // update db
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => widget.reload(),
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 1,
                            blurRadius: 5,
                            offset: Offset(0, 0), // changes position of shadow
                          ),
                        ],
                      ),
                      width: double.infinity,
                      child: Text(
                        widget.item.isNotEmpty && widget.item.length > 0
                            ? widget.item['name']
                            : 'Loading',
                        style: TextStyle(
                          // fontWeight: FontWeight.bold,
                          fontSize: 21,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    getTextWidgets(
                        widget.userData, widget.item['exercises'], context),
                  ],
                ),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.4),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 0), // changes position of shadow
                ),
              ],
            ),
            width: double.infinity,
            height: 50,
            child: SubmitButton(
              title: 'Complete Workout',
              onPressed: () => widget.completeWorkout(),
              bg: Colors.pinkAccent[400],
              radius: 0,
            ),
          ),
        ],
      ),
    );
  }

  Widget getTextWidgets(
      userData, List<dynamic> exercises, BuildContext context) {
    String _calcWeight(e) {
      var orm = e['oneRepMax'];

      // ::todo choose which to increment
      if (e['exercise'] == 'Squat') orm = userData.squatORM;
      if (e['exercise'] == 'Bench') orm = userData.benchORM;
      if (e['exercise'] == 'Deadlift') orm = userData.deadliftORM;
      if (e['exercise'] == 'Calf-Raises') orm = userData.calfRaisesORM;
      if (e['exercise'] == 'Glute-Bridge') orm = userData.gluteBridgeORM;
      if (e['exercise'] == 'Curl' ||
          e['exercise'] == 'Tricep Extensions' ||
          e['exercise'] == 'Skull-Crusher') orm = userData.curlORM;
      if (e['exercise'] == 'Row') orm = userData.rowORM;
      if (e['exercise'] == 'Uppercuts') orm = userData.uppercutORM;
      if (e['exercise'] == 'Shoulder-Press') orm = userData.pressORM;
      if (e['exercise'] == 'Close-Grip-Bench') orm = userData.closeORM;
      if (e['exercise'] == 'Romanian-Deadlift') orm = userData.rdlORM;
      if (e['exercise'] == 'Incline-Bench') orm = userData.inclineORM;
      if (e['exercise'] == 'Lat-Raises') orm = userData.latORM;
      if (e['exercise'] == 'Shrug') orm = userData.shrugORM;

      double w = orm * (e['weightPercentage'] / 100);
      w = (w / 5).round() * 5 + .0; // round to nearest 5
      return w.round().toString();
    }

    return Container(
      width: double.infinity,
      child: Container(
        constraints: BoxConstraints(minHeight: 100, minWidth: double.infinity),
        child: new Column(
            children: exercises.map(
          (exercise) {
            // var index = exercises.indexOf(exercise);
            return WorkoutCard(
              exercise: exercise,
              calcWeight: _calcWeight,
              updateSuccess: _updateSuccess,
              reload: widget.reload,
            );
          },
        ).toList()),
      ),
    );
  }
}
