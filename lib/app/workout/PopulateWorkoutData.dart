dynamic getData(id) {
  print(id);

  switch (id) {
    case 'bike-01':
      return {
        'alternatives': 'Swimming, Rollerblading, Rowing, Hiking, Skiing',
        'description':
            'Indoor, outdoor, doesn\'t matter. Hop on a bike and lets go for a ride.  Rollerblading, skateboarding, rowing, you  can substitute anything.\n\nBiking is great if you have issues with your knees or back as it\'s lower impact. Finding a nice trail can also give you some time to take in nature and clear your head.',
        'url':
            'https://www.cyclingweekly.com/news/latest-news/benefits-of-cycling-334144',
        'buy': null,
        'video': 'x2nF7QJgGvY',
      };
      break;

    case 'bike-02':
      return {
        'alternatives': 'Swimming, Rollerblading, Rowing, Hiking, Skiing',
        'description':
            'Indoor, outdoor, doesn\'t matter. Hop on a bike and lets go for a ride.  Rollerblading, skateboarding, rowing, you  can substitute anything.\n\nBiking is great if you have issues with your knees or back as it\'s lower impact. Finding a nice trail can also give you some time to take in nature and clear your head.',
        'url':
            'https://www.cyclingweekly.com/news/latest-news/benefits-of-cycling-334144',
        'buy': null,
        'video': 'WKK7_6RZZNM',
      };
      break;

    case 'bike-03':
      return {
        'alternatives': 'Swimming, Rollerblading, Rowing, Hiking, Skiing',
        'description':
            'Indoor, outdoor, doesn\'t matter. Hop on a bike and lets go for a ride.  Rollerblading, skateboarding, rowing, you  can substitute anything.\n\nBiking is great if you have issues with your knees or back as it\'s lower impact. Finding a nice trail can also give you some time to take in nature and clear your head.',
        'url':
            'https://www.cyclingweekly.com/news/latest-news/benefits-of-cycling-334144',
        'buy': null,
        'video': 'vzI7WGSAA64',
      };
      break;
    case 'close-01':
      return {
        'alternatives': 'Dips, tricep extensions, tricep pulldowns',
        'description':
            'This bench is not made for working your chest but your triceps. Move your hands closer than a standard bench, but not TOO close together. Bring the weight down, while keeping  your  elbows  tucked close to your body. Pause and then push back up.',
        'url':
            'https://www.coachmag.co.uk/triceps-exercises/7483/how-to-do-the-close-grip-bench-press',
        'buy': null,
        'video': 'vEUyEOVn3yM',
      };
      break;

    case 'curl-wu':
      return {
        'alternatives':
            'Band curls, Concentration Curls, Seated Curls, Hammer Curls',
        'description':
            'Curls work the glamor muscles, sure, but we can combine isolation exercises with our other main lifts to help our smaller muscles pop. Curls alone wont give you big arms, if that\'s what you\'re looking for, but they will help with that "peak". Consider splitting these exercises with dips or tricep extensions.',
        'url': 'https://www.masterclass.com/articles/dumbbell-curl-guide',
        'buy': null,
        'video': 'm3Nsjdx-UY0',
      };
      break;
    case 'curl-01':
      return {
        'alternatives':
            'Band curls, Concentration Curls, Seated Curls, Hammer Curls',
        'description':
            'You don\'t want to overtrain your biceps. You also want to make sure you focus completely on using the biceps to move the weight. Don\'t swing it, and  don\'t let your elbows drift. Keep them tucked at ur sides, arms straight, and bend at the elbow to bring them up. Consider alternating standing/seated curls  at 45 degrees to target both portions of the muscle. Again, sprinkle in some tricep exercises if you want to get bigger looking arms.',
        'url':
            'https://www.masterclass.com/articles/barbell-curl-guide#what-is-a-barbell-curl',
        'buy': null,
        'video': 'JJB8XgKltA8',
      };
      break;
    case 'rdl-01':
      return {
        'alternatives': 'Rack pulls, deadlift, good mornings',
        'description':
            'Unlike the standard deadlift, you\'ll keep your legs close together for this one.  Focus on pushing your hips backwards, and you will really feel it in the hamstrings. Slowly lock out at the top, keeping your back straight. Pause, lower with control.',
        'url':
            'https://www.healthline.com/health/romanian-deadlift-vs-deadlift',
        'buy': null,
        'video': '2SHsk9AzdjA',
      };
      break;

    case 'incline-01':
      return {
        'alternatives': 'Incline flys, standing cable flys',
        'description':
            'This workout is very similar to the standard bench, but will focus on your upper chest. Being inclined means you probably want to go slightly lighter than your normal bench. ',
        'url':
            'https://www.anabolicaliens.com/blog/incline-dumbbell-press-exercise',
        'buy': null,
        'video': 'SrqOu55lrYU',
      };
      break;

    case 'tricep-01':
      return {
        'alternatives':
            'Skull Crushers, Kickback, Close Grip Bench, Pulldowns, Dips',
        'description':
            'People think "Big arms" means big biceps. But honestly, you need the whole package to have truly big arms. Focus on the squeeze and make sure you don\'t over do it. If you don\'t have cables or bands, you can substitute kickbacks or whatever else listed above.',
        'url':
            'https://www.stack.com/a/for-bigger-arms-and-better-performance-train-your-triceps/',
        'buy': null,
        'video': 'X-iV-cG8cYs',
      };
      break;

    case 'hacky-01':
      return {
        'alternatives': ['Skateboard', 'Snowboard', 'Frolf', 'Racquet Ball'],
        'description':
            'Dood, just go have fun. Try to top your best each time and try to raise your avg hits before losing it.',
        'url':
            'https://www.amazon.com/World-Footbag-Dirtbag-Hacky-Sack/dp/B07FVWGQF1/ref=sr_1_4?keywords=Dirtbag&qid=1641171906&sr=8-4',
        'buy': null,
        'video': '_MfoxBPT-YY',
      };
      break;

    case 'gb-wu':
      return {
        'alternatives':
            'Reverse Lunges, Band Squat Hold, Kettle Bell Swings, Dumbell Roman Deadlift',
        'description':
            'You can do this on a bench or flat on the floor. We prefer the ground variety but to each their own. Be careful when loading & unloading the weight.\n\nFocus on contracting your butt as you push through your heels to raise your hips. Pause at the top and repeat.',
        'url': 'https://darkironfitness.com/benefits-of-glute-bridges/',
        'buy': null,
        'video': 'nbjJjSa0cKo',
      };
      break;
    case 'gb-01':
      return {
        'alternatives':
            'Reverse Lunges, Band Squat Hold, Kettle Bell Swings, Dumbell Roman Deadlift',
        'description':
            'Now we\'re going to be getting heavier. Really focus on keeping your heels grounded and flexing your hips up. Push through the floor until your  glutes are tight. Keep your shoulders flat and your chin up.\n\nTry to not toss the weight up. The slower you  go, the more you\'ll feel it. Pause at the top for about 2-3 seconds. Lower down slowly then repeat the motion.',
        'url': 'https://www.healthline.com/health/barbell-glute-bridge#how-to',
        'buy': null,
        'video': 'xDmFkJxPzeM',
      };
      break;
    case 'jog':
      return {
        'alternatives': 'Eliptical, Row Machine, Bike, Swimming, Jump Rope',
        'buy': null,
        'description':
            'Get your blood flowing before we get started with our workout. This is a nice easy jog, not a sprint. You want to elevate your heart rate slightly, get your body limber, and let your muscles know something is coming.\n\nHate running? That\'s fine...Jump some rope, do jumping jacks, just get your heart rate up.\n\nWarming up preps your bod for what is to come, and also lubricates the joints, and will help if you have any stiffness or soreness.',
        'url': 'https://www.goldsgym.com/blog/warm-lifting-weights/',
        'video': '_kGESn8ArrU',
      };
      break;
    case 'press-01':
      return {
        'description':
            'Get ready to work your upper body. The shoulder press or overhead press can be performed standing or sitting, your preference. start with the weights around your ears, and push up, pause at the top, lower, repeat.',
        'url':
            'https://www.coachmag.co.uk/shoulder-exercises/6134/how-to-do-a-dumbbell-shoulder-press',
        'buy': null,
        'video': 'B-aVuyhvLHU',
      };
      break;

    case 'stretch-01':
      return {
        'description':
            'Grab a foam roller, tennis ball, or impact massage gun and hit anywhere that is tight.\n\nFoam rolling really helps with the spine and t-bands to loosen up the lower back (and the  entire body really). You\'ll decrease muscle soreness, improve flexibility/range of motion, and it just feels good! \n\nCheck out the "TriggerPoint GRID Foam Roller" on Amazon as the roller we prefer.',
        'url':
            'https://www.healthline.com/health/fitness-exercise/foam-rolling-how-to',
        'buy':
            'https://www.amazon.com/gp/product/B08GP1W8ZT/ref=ppx_yo_dt_b_search_asin_image?ie=UTF8&th=1',
        'video': 'aTcRYJsuhkI',
      };
      break;
    case 'stretch-02':
      return {
        'description':
            'Do some dynamic stretching focusing on some pain points. A quick yoga flow is good, or just stretch and hold whatever feels good.',
        'url':
            'https://www.healthline.com/health/exercise-fitness/dynamic-stretching#for-runners',
        'buy': null,
        'video': 'uQ2yJhF4zZY',
      };
      break;
    case 'stretch-03':
      return {
        'description':
            'Focus on lower-back and hamstrings. Perform toe touches, hamstring stretches, dead bugs, windshield whipers, whatever feels good...do more of that.',
        'url':
            'https://www.healthline.com/health/exercise-fitness/dynamic-stretching#for-runners',
        'buy': null,
        'video': 'uQ2yJhF4zZY',
      };
      break;
    case 'bench-wu':
      return {
        'alternatives':
            'Chest Press, Pushups, Incline/Decline Bench, Dumbell Flys, Cable Cross-Overs',
        'description':
            'Take a light weight, focus on form, and slow down. Think about squeezing your pecks. Retract your scapula. Our warmup sets start very light, so you can focus on proper technique, and get heavier until we hit our working weight.\n\nFeel free to add some extra warmups if you lift heavy.\n\nYou don\'t need much, if any, break in between sets.',
        'url':
            'https://www.coachmag.co.uk/exercises/chest-exercises/186/bench-press-technique-tips',
        'buy': null,
        'video': 'gRVjAtPip0Y',
      };
      break;

    case 'bench-01':
      return {
        'alternatives':
            'Chest Press, Pushups, Incline/Decline Bench, Dumbell Flys, Cable Cross-Overs',
        'description':
            'Now  we\'re on to our working sets. Maintain your form and focus on your pecs not your shoulders or back. Be mindful of which muscles are at work. Your arms should be around shoulder width, plant your feet to the floor, eye/chest at the sky, shoulders pinched together. Slowly lower down to your nips. Brace your butt and abs as you lower, pause at the bottom, then push up. Repeat. Use a spotter if you lift heavy.',
        'url':
            'https://www.bodybuilding.com/content/how-to-bench-press-layne-norton-complete-guide.html',
        'buy': null,
        'video': 'vcBig73ojpE',
      };
      break;
    case 'boxing-01':
      return {
        'alternatives':
            'Jump Rope, Shadow Boxing, HiiT Workout, Battle Ropes, Med. Ball',
        'description':
            'Boxing can be so much fun. If you don\'t have a heavy bag, don\'t worry. You can still shadow box. There are a lot of videos on YouTube you can follow. Or you can just watch classic matches and mimic what they do.\n\nAli, Tyson, Joshua, Pacquiao, Alvarez, Spence, so many great boxers to watch. Try kicking the speed up to 1.5x and pretend you\'re in the ring. Keep moving, faint, jab, enjoy it.',
        'url': 'https://expertboxing.com/the-beginners-guide-to-boxing',
        'buy': null,
        'video': 'zDoKRPGue7M',
      };
      break;

    case 'calf-wu':
      return {
        'alternatives': 'Seated Calf-Raise, Jump Rope, Farmers Walk, Box Jumps',
        'description':
            'Calf raises are an isolation exercise. We focus on a specific muscle, that is sometimes overlooked. But the benefits can be pretty drastic, increased mobility, jumping height, and jump rope endurance are just some benefits.\n\nPaired with a routine that includes squates and other lower body exercises will also help reduce falls as you get older. Longevity is related to leg strength and balance and our legs consist of several muscles. We don\'t want to overlook any.',
        'url':
            'https://www.menshealth.com/uk/building-muscle/a36045873/calf-raises-workouts/',
        'buy': null,
        'video': 'Xa18jxyeSnM',
      };
      break;

    case 'calf-01':
      return {
        'alternatives': 'Seated Calf-Raise, Jump Rope, Farmers Walk, Box Jumps',
        'description':
            'Because an isolation exercise doesn\'t work more than one muscle, we\'ll do more reps with less sets to really feel the burn. But just because we\'re not doing as many sets doesn\'t mean you shouldn\'t concentrate. If you forget to focus on form you won\'t get the  benefits.\n\nTake these extra slow on the way down and explode on the way up. Keep your feet straight. You can either hold dumbells or put the weight on your shoulders like a squat if you go heavy.',
        'url':
            'https://www.healthshots.com/fitness/muscle-gain/5-benefits-of-calf-raises/',
        'buy': null,
        'video': '5xpI4HtUCzs',
      };
      break;

    case 'deadlift-wu':
      return {
        'alternatives':
            'Rack Pulls, Glute Bridge / Hip Thrusters, Trap Bar, Roman Deadlift, Good Mornings, Pistol Squat, Kettle Bell',
        'description':
            'Deadlifts work several muscle groups, as well as give you a little cardio boost, and can help with your balance. We warm up and really need to focus on form because deadlifts are also a workout where people injure themselves.\n\nThat\'s not to say DLs are dangerous, or more  dangerous than squats or other lifts. But if your form isn\'t right, your chances of hurting your back are pretty high.\n\nJust stay focused, have proper form, take your time, and don\'t over do it.',
        'url': 'https://www.healthline.com/health/fitness/deadlift-benefits',
        'buy': null,
        'video': '4qRntuXBSc',
      };
      break;

    case 'deadlift-01':
      return {
        'alternatives':
            'Rack Pulls, Glute Bridge / Hip Thrusters, Trap Bar, Roman Deadlift, Good Mornings, Pistol Squat, Kettle Bell',
        'description':
            'We have less sets of the DL because we honestly don\'t need more. We take our time, we lift heavy, and we lift mindful. Focus on your breathing and your form. Don\'t jerk the weight, lift it with your lower body. The better your form, them bigger the gains.\n\nKeep your feet about shoulder width apart, grab the bar a little  wider than your feet. Pick up the bar, dirving your hips forward and a straight back. Stand up, contracting your muscles. Lower controlled. Repeat.',
        'url':
            'https://www.coachmag.co.uk/barbell-exercises/3725/how-to-deadlift',
        'buy': null,
        'video': 'VL5Ab0T07e4',
      };
      break;

    case 'row-wu':
      return {
        'alternatives': 'Pull-ups, Snatch, Upright Row, Lat Raises',
        'description':
            'Barbell rows will help build a bigger back. There are a lot of different versions, but form is the most important part so as to not injure yourself no matter which form you choose. First, step up to the bar, close like  the deadlift, or from  the bottom run on a rack. Angle your back and keep it straight and grab the bar just about shoulder width. Pull the bar up while keeping your elbows close to your body. Try to pull the bar towards the bottom of your ribs so your elbows stick out past your spine.\n\nYou might want to try to "twist" the bar out as if you wanted to bend it. Squeeze your shoulders together, pause, and slowly lower back to the starting postion. Repeat.',
        'url': 'https://stronglifts.com/barbell-row/',
        'buy': null,
        'video': 'T3N-TO4reLQ',
      };
      break;

    case 'row-01':
      return {
        'alternatives': 'Pull-ups, Snatch, Upright Row, Lat Raises',
        'description':
            'As you get heavier, your form will suffer. You do not want to feel it in your biceps or your lower back. If you do, your form is off. Keep your hips back and your back straight. Flex at the knees and pull the bar up. Don\'t jerk or swing it up.\n\nStart with low weight, and work your way up as your form gets better. Just like with all lifts, check your ego. Heavy weights with bad form won\'t help anyone.',
        'url': 'https://www.kingofthegym.com/benefits-of-barbell-rows/',
        'buy': null,
        'video': 'kBWAon7ItDw',
      };
      break;

    case 'lat-raises-01':
      return {
        'description':
            'Lateral raises are an isolation exercise for your shoulders. This exercise will help you get that full look to your upperbody, making your shirts fit better. Feel free to very the angle to hit different parts of the muscle.',
        'url': 'https://www.healthline.com/health/rear-lateral-raises',
        'buy': null,
        'video': 'v_ZkxWzYnMc',
      };
      break;

    case 'run-01':
      return {
        'alternatives': 'Eliptical, Row Machine, Bike, Swimming, Jump Rope',
        'description':
            'Take it easy, shoot for 3 mi or around 30 min, but do whatever pace FEELS good. Try for a 4/10 effort. Can you bump it up? Sure! But can you finish as strong as you started? If the answer is "No", then keep  a lower effort. And if you get started, and you think you can\'t finish at that pace, take it bakc a notch.\n\nWhy do most people hate running? Because they start off to hard and get gassed too quickly. As you do more  of these runs, you\'ll notice they get easier and easier. And eventually, maybe 30 min is too short, or 4/10 is not enough effort. Build up at YOUR pace.\n\nIf you have bad knees or prefer another  cardio workout, try swimming or a bike. Also, Nike Run Club is a great companion.',
        'url':
            'https://www.letsdothis.com/blog/7-benefits-of-running-for-just-30-minutes/',
        'buy': null,
        'video': 'Dy28eq2PjcM',
      };
      break;

    case 'shrugs-01':
      return {
        'description':
            'Targeting your traps, the shrug will give you an overall stronger base, which should help gains with other upperbody exercises. On your inhale, shrug the weights up by brining your shoulders towards your ears. Keep your chin up and head straight. Pause, lower, repeat.',
        'url': 'https://www.menshealth.com/fitness/a31227286/shrugs-exercise/',
        'buy': null,
        'video': 'NAqCVe2mwzM',
      };
      break;

    case 'sports-01':
      return {
        'alternatives':
            'Basketball, Volleyball, Soccer, Swimming, Hockey, Tennis/Racquet Ball, Football, ',
        'description':
            'Grab a basketball and hit the court, or your basement, or driveway, or wherever. You can play a pickup game or just dribble around your house at a decent pace. Practice makes perfect, so if you can\'t get to a court, just practice your ball handling or shooting. Get your heart rate up and just enjoy yourself.\n\nThis will help with your cardio, but also your hand eye coordination. Just like always, feel free to swap it out for any other sport like soccer or football. Maybe learn a neat skill like dribbling through your legs, spinning on your finder, etc.',
        'url':
            'https://www.dailyhealthybody.com/fitness/playing-basketball-health-benefits/',
        'buy': null,
        'video': '1K0rlzOXOZY',
      };
      break;
    case 'sports-02':
      return {
        'alternatives':
            'Basketball, Volleyball, Soccer, Swimming, Hockey, Tennis/Racquet Ball, Football, ',
        'description':
            'Grab a basketball and hit the court, or your basement, or driveway, or wherever. You can play a pickup game or just dribble around your house at a decent pace. Practice makes perfect, so if you can\'t get to a court, just practice your ball handling or shooting. Get your heart rate up and just enjoy yourself.\n\nThis will help with your cardio, but also your hand eye coordination. Just like always, feel free to swap it out for any other sport like soccer or football. Maybe learn a neat skill like dribbling through your legs, spinning on your finder, etc.',
        'url':
            'https://www.dailyhealthybody.com/fitness/playing-basketball-health-benefits/',
        'buy': null,
        'video': '8d-4AxWdUhA',
      };
      break;
    case 'sports-03':
      return {
        'alternatives':
            'Basketball, Volleyball, Soccer, Swimming, Hockey, Tennis/Racquet Ball, Football, ',
        'description':
            'Grab a basketball and hit the court, or your basement, or driveway, or wherever. You can play a pickup game or just dribble around your house at a decent pace. Practice makes perfect, so if you can\'t get to a court, just practice your ball handling or shooting. Get your heart rate up and just enjoy yourself.\n\nThis will help with your cardio, but also your hand eye coordination. Just like always, feel free to swap it out for any other sport like soccer or football. Maybe learn a neat skill like dribbling through your legs, spinning on your finder, etc.',
        'url':
            'https://www.dailyhealthybody.com/fitness/playing-basketball-health-benefits/',
        'buy': null,
        'video': 'HRGU3igMytM',
      };
      break;

    case 'squat-wu':
      return {
        'alternatives':
            'Seat-Supported Squats, Roman Deadlift, Leg Press, Lunges, Kettle Bell, Just do squats',
        'description':
            'Take a light weight, focus on form, and slow down. Warmup squats will tell your legs and back that something bigger is coming. Our warmup sets start very light, so you can focus on proper technique, and get heavier until we hit our working weight.\n\nFeel free to add some extra if you lift heavy.\n\nYou don\'t need much, if any, break in between sets.',
        'url':
            'https://barbend.com/best-way-to-warm-up-before-lifting-weights/',
        'buy': null,
        'video': 'Dy28eq2PjcM',
      };
      break;

    case 'squat-01':
      return {
        'alternatives':
            'Seat-Supported Squats, Roman Deadlift, Leg Press, Lunges, Kettle Bell, Just do squats',
        'description':
            'Now  we\'re on to our working sets. Maintain your form. Be mindful of which muscles are at work. Spread your legs out, plant your feet to the floor, eye/chest up, slowly lower down to just below parallel (use a bench if you have trouble or an old injury). Brace your butt and abs as you lower, pause at the bottom, and push through your heels on the way up. Repeat.',
        'url':
            'https://www.self.com/story/5-ways-youre-probably-doing-squats-wrong',
        'buy': null,
        'video': 'bEv6CCg2BC8',
      };
      break;
    case 'uppercut-wu':
      return {
        'alternatives':
            'Cable Cross-Overs, Dumbell Flys, Front Lat Raises, Chest Dips, Pushups, Band Cross-Overs',
        'description':
            'Uppercuts or standing flys are great isolation exercises for targeting both your lower and upper chest to give you that nice looking shape. You will want to go light on these and really focus on the contraction at the top. We\'re doing high reps with low sets so really concentrate.\n\nYou can do this with dumbells or a cable setup. ',
        'url': 'https://homegym-exercises.com/uppercut_dumbbells.html',
        'buy': null,
        'video': 'byxD_y2e_L8',
      };
      break;

    case 'uppercut-01':
      return {
        'alternatives':
            'Cable Cross-Overs, Dumbell Flys, Front Lat Raises, Chest Dips, Pushups, Band Cross-Overs',
        'description':
            'On our working sets we want to make sure we do not go TOO heavy. Again focus on form and don\'t swing the weight up. There are many different names for this, and there are lots of exercises that can be swapped out like cable flys, front dips, cable cross-overs, or regular flat bench flys.',
        'url': 'https://homegym-exercises.com/uppercut_dumbbells.html',
        'buy': null,
        'video': 'zgq0jPU5YAQ',
      };
      break;

    case 'yoga-01':
      return {
        'alternatives': 'Pilates, Swimming, Foam Roller, Rock Climbing, BJJ',
        'description':
            'Yoga is great. If you want to get more limber and flexible, or work on some nagging tightness, yoga has you covered. Make sure your breathing is controlled the whole time. Engage your  core (meaning flex it and pull your bellybutton towards your spine. Do the moves slowly and get a deep stretch or speed it up for a mild cardio. Hold the positions that you feel tight in for longer and sink deeper to really release that tension.\n\nDo your own thing, if something feels good, stay  there longer. If something starts to feel bad, STOP. Try to figure out why you have pain and adapt.',
        'url': 'https://yogawithadriene.com/',
        'buy': null,
        'video': 'bEv6CCg2BC8',
      };
      break;
    case 'yoga-02':
      return {
        'alternatives': 'Pilates, Swimming, Foam Roller, Rock Climbing, BJJ',
        'description':
            'Yoga is great. If you want to get more limber and flexible, or work on some nagging tightness, yoga has you covered. Make sure your breathing is controlled the whole time. Engage your  core (meaning flex it and pull your bellybutton towards your spine. Do the moves slowly and get a deep stretch or speed it up for a mild cardio. Hold the positions that you feel tight in for longer and sink deeper to really release that tension.\n\nDo your own thing, if something feels good, stay  there longer. If something starts to feel bad, STOP. Try to figure out why you have pain and adapt.',
        'url': 'https://yogawithadriene.com/',
        'buy': null,
        'video': 'I-bG5E00PPY',
      };
    case 'yoga-03':
      return {
        'alternatives': 'Pilates, Swimming, Foam Roller, Rock Climbing, BJJ',
        'description':
            'Yoga is great. If you want to get more limber and flexible, or work on some nagging tightness, yoga has you covered. Make sure your breathing is controlled the whole time. Engage your  core (meaning flex it and pull your bellybutton towards your spine. Do the moves slowly and get a deep stretch or speed it up for a mild cardio. Hold the positions that you feel tight in for longer and sink deeper to really release that tension.\n\nDo your own thing, if something feels good, stay  there longer. If something starts to feel bad, STOP. Try to figure out why you have pain and adapt.',
        'url': 'https://yogawithadriene.com/',
        'buy': null,
        'video': 'eiQjt0WSVHk',
      };
      break;
    default:
      return {
        'description': '',
        'url': null,
        'video': null,
      };
  }
}
