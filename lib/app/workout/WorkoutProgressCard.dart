import 'package:flutter/material.dart';

class WorkoutProgressCard extends StatefulWidget {
  final int currentWorkoutIndex;
  final int index;
  final dynamic exercise;

  WorkoutProgressCard({
    this.currentWorkoutIndex,
    this.index,
    this.exercise,
  });

  @override
  _WorkoutProgressCardState createState() => _WorkoutProgressCardState();
}

class _WorkoutProgressCardState extends State<WorkoutProgressCard> {
  bool showMore = false;

  @override
  Widget build(BuildContext context) {
    int currentWorkoutIndex = widget.currentWorkoutIndex;
    int index = widget.index;
    dynamic exercise = widget.exercise;

    return Container(
      decoration: BoxDecoration(
        border: new Border(bottom: new BorderSide(color: Colors.grey[200])),
        color: currentWorkoutIndex == index ? Colors.black : Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black45,
            blurRadius: currentWorkoutIndex == index ? 8 : 0,
            offset: const Offset(0, 0),
          ),
        ],
      ),
      constraints: BoxConstraints(minHeight: 50, minWidth: double.infinity),
      child: Align(
        alignment: Alignment.topLeft,
        child: Column(
          children: [
            ListTile(
              leading: Icon(
                currentWorkoutIndex > index
                    ? Icons.check_circle_outline_outlined
                    : Icons.circle_outlined,
                color:
                    currentWorkoutIndex == index ? Colors.white : Colors.black,
              ),
              trailing: IconButton(
                onPressed: () {
                  setState(() => showMore = !showMore);
                },
                icon: Icon(showMore == true
                    ? Icons.arrow_drop_up_outlined
                    : Icons.arrow_drop_down_outlined),
                color: currentWorkoutIndex == index
                    ? Colors.white
                    : Colors.grey[300],
              ),
              title: Text(
                exercise['name'],
                style: TextStyle(
                  //fontWeight: FontWeight.bold,
                  color: currentWorkoutIndex == index
                      ? Colors.white
                      : Colors.black,
                ),
              ),
            ),
            showMore == true
                ? Container(
                    color: Colors.grey[100],
                    child: Column(
                        children: exercise['exercises'].map<Widget>(
                      (ex) {
                        // var index = exercises.indexOf(exercise);
                        return (ex['sets'] == 0 ||
                                ex['name'] == 'Warm-up' ||
                                ex['name'] == 'Mobility')
                            ? SizedBox(height: 0)
                            : ListTile(
                                trailing: ex['weightPercentage'] != null
                                    ? Text('${ex['weightPercentage']}% ORM')
                                    : Text('${ex['timeInMin']} Min'),
                                title: Text(ex['name'],
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                subtitle: Text(
                                    'Set: ${ex['sets'].toString()} / Reps: ${ex['reps'].toString()}'),
                              );
                      },
                    ).toList()),
                  )
                : SizedBox(height: 0),
          ],
        ),
      ),
    );
  }
}
