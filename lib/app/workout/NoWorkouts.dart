import 'package:flutter/material.dart';
import 'package:nerd_fit/app/home/TabItem.dart';

class NoWorkouts extends StatefulWidget {
  NoWorkouts({this.onSelectTab});
  final ValueChanged<TabItem> onSelectTab;

  @override
  State<NoWorkouts> createState() => _NoWorkoutsState();
}

class _NoWorkoutsState extends State<NoWorkouts> {
  bool showStepper = false;
  int _currentStep = 0;
  StepperType stepperType = StepperType.vertical;

  @override
  Widget build(BuildContext context) {
    // final userData =
    //               await Provider.of<Database>(context, listen: false)
    //                   .getUserDataStream()
    //                   .first;

    tapped(int step) {
      setState(() => _currentStep = step);
    }

    continued() {
      _currentStep < 2 ? setState(() => _currentStep += 1) : null;
    }

    cancel() {
      _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
    }

    return (showStepper != true)
        ? GestureDetector(
            onTap: () {
              print('move it');
              //widget.onSelectTab(TabItem.profile);
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  '+',
                  style: TextStyle(
                    fontSize: 50,
                  ),
                ),
                Text(
                  'Create a routine',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          )
        : Center(
            child: Stepper(
              type: stepperType,
              physics: ScrollPhysics(),
              currentStep: _currentStep,
              onStepTapped: (step) => tapped(step),
              onStepContinue: continued,
              onStepCancel: cancel,
              steps: <Step>[
                Step(
                  title: new Text('Account'),
                  content: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Email Address'),
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Password'),
                      ),
                    ],
                  ),
                  isActive: _currentStep >= 0,
                  state: _currentStep >= 0
                      ? StepState.complete
                      : StepState.disabled,
                ),
                Step(
                  title: new Text('Address'),
                  content: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Home Address'),
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Postcode'),
                      ),
                    ],
                  ),
                  isActive: _currentStep >= 0,
                  state: _currentStep >= 1
                      ? StepState.complete
                      : StepState.disabled,
                ),
                Step(
                  title: new Text('Mobile Number'),
                  content: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Mobile Number'),
                      ),
                    ],
                  ),
                  isActive: _currentStep >= 0,
                  state: _currentStep >= 2
                      ? StepState.complete
                      : StepState.disabled,
                ),
              ],
            ),
          );
  }
}
