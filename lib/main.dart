import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// ## nwc content (authorization, styles, routes)
import 'package:nerd_fit/providers/auth.dart';
import 'package:nerd_fit/components/ThemeData.dart';
import 'package:nerd_fit/screens/LandingPageScreen.dart';

void main() => runApp(NerdFitApp());

class NerdFitApp extends StatelessWidget {
  const NerdFitApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider<AuthBase>(
      create: (context) => Auth(),
      child: MaterialApp(
        home: LandingPageScreen(),
        theme: myThemeData(),
        title: 'Nerd Fit',
      ),
    );
  }
}
